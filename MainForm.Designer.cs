﻿namespace TmlGogPatcher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._textPath = new System.Windows.Forms.TextBox();
            this._textLog = new System.Windows.Forms.TextBox();
            this._buttonInstall = new System.Windows.Forms.Button();
            this._comboVersionTer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._comboVersionTml = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._checkOverwriteClient = new System.Windows.Forms.CheckBox();
            this._checkOverwriteServer = new System.Windows.Forms.CheckBox();
            this._groupMisc = new System.Windows.Forms.GroupBox();
            this._checkInstallDependencies = new System.Windows.Forms.CheckBox();
            this._checkInstallServer = new System.Windows.Forms.CheckBox();
            this._checkInstallClient = new System.Windows.Forms.CheckBox();
            this._groupInstall = new System.Windows.Forms.GroupBox();
            this._picDirPathStatus = new System.Windows.Forms.PictureBox();
            this._buttonBrowse = new System.Windows.Forms.Button();
            this._buttonOpen = new System.Windows.Forms.Button();
            this._tips = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._stripTools = new System.Windows.Forms.ToolStrip();
            this._toolAbout = new System.Windows.Forms.ToolStripButton();
            this._separator3 = new System.Windows.Forms.ToolStripSeparator();
            this._toolVisit = new System.Windows.Forms.ToolStripDropDownButton();
            this._toolVisitPatcher = new System.Windows.Forms.ToolStripMenuItem();
            this._toolVisitPatcherWebsite = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitPatcherThread = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitPatcherGitlab = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitPatcherWiki = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTerraria = new System.Windows.Forms.ToolStripMenuItem();
            this._toolVisitTerrariaWebsite = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTerrariaForum = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTerrariaForumRules = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTerrariaModRules = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTml = new System.Windows.Forms.ToolStripMenuItem();
            this._toolVisitTmlThread = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTmlGithub = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolVisitTmlWiki = new TmlGogPatcher.Controls.ToolStripUrlMenuItem();
            this._toolFolder = new System.Windows.Forms.ToolStripDropDownButton();
            this._toolOpenFoldersPatcher = new System.Windows.Forms.ToolStripMenuItem();
            this._toolOpenFolderPatcher = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderPatcherLogs = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderPatcherPatches = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFoldersTerraria = new System.Windows.Forms.ToolStripMenuItem();
            this._toolOpenFolderTerraria = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTerrariaCaptures = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTerrariaLogs = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTerrariaPlayers = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTerrariaWorlds = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFoldersTml = new System.Windows.Forms.ToolStripMenuItem();
            this._toolOpenFolderTml = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlCaptures = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlLogs = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlMods = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlSources = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlPlayers = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolOpenFolderTmlWorlds = new TmlGogPatcher.Controls.ToolStripPathMenuItem();
            this._toolViewLog = new System.Windows.Forms.ToolStripButton();
            this._toolPatches = new System.Windows.Forms.ToolStripDropDownButton();
            this._toolPatchDownload = new System.Windows.Forms.ToolStripMenuItem();
            this._toolPatchList = new System.Windows.Forms.ToolStripMenuItem();
            this._stripStatus = new System.Windows.Forms.StatusStrip();
            this._toolRefresh = new System.Windows.Forms.ToolStripDropDownButton();
            this._status = new System.Windows.Forms.ToolStripStatusLabel();
            this._progress = new System.Windows.Forms.ToolStripProgressBar();
            this._groupMisc.SuspendLayout();
            this._groupInstall.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picDirPathStatus)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._stripTools.SuspendLayout();
            this._stripStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // _textPath
            // 
            this._textPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textPath.Location = new System.Drawing.Point(29, 13);
            this._textPath.Margin = new System.Windows.Forms.Padding(2);
            this._textPath.Name = "_textPath";
            this._textPath.Size = new System.Drawing.Size(275, 20);
            this._textPath.TabIndex = 0;
            this._textPath.TextChanged += new System.EventHandler(this._textPath_TextChanged);
            // 
            // _textLog
            // 
            this._textLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textLog.BackColor = System.Drawing.SystemColors.Window;
            this._textLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textLog.Location = new System.Drawing.Point(0, 175);
            this._textLog.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this._textLog.Multiline = true;
            this._textLog.Name = "_textLog";
            this._textLog.ReadOnly = true;
            this._textLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._textLog.Size = new System.Drawing.Size(384, 115);
            this._textLog.TabIndex = 5;
            this._textLog.WordWrap = false;
            // 
            // _buttonInstall
            // 
            this._buttonInstall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._buttonInstall.Location = new System.Drawing.Point(6, 11);
            this._buttonInstall.Name = "_buttonInstall";
            this._buttonInstall.Size = new System.Drawing.Size(348, 24);
            this._buttonInstall.TabIndex = 0;
            this._buttonInstall.Text = "&Install";
            this._tips.SetToolTip(this._buttonInstall, "Install tModLoader based on the selected options.");
            this._buttonInstall.UseVisualStyleBackColor = true;
            this._buttonInstall.Click += new System.EventHandler(this._buttonInstall_Click);
            // 
            // _comboVersionTer
            // 
            this._comboVersionTer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboVersionTer.FormattingEnabled = true;
            this._comboVersionTer.Location = new System.Drawing.Point(50, 16);
            this._comboVersionTer.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this._comboVersionTer.Name = "_comboVersionTer";
            this._comboVersionTer.Size = new System.Drawing.Size(60, 21);
            this._comboVersionTer.TabIndex = 1;
            this._tips.SetToolTip(this._comboVersionTer, "Select Terraria version.");
            this._comboVersionTer.SelectedIndexChanged += new System.EventHandler(this._comboVersionTer_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Terraria:";
            // 
            // _comboVersionTml
            // 
            this._comboVersionTml.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboVersionTml.FormattingEnabled = true;
            this._comboVersionTml.Location = new System.Drawing.Point(148, 16);
            this._comboVersionTml.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this._comboVersionTml.Name = "_comboVersionTml";
            this._comboVersionTml.Size = new System.Drawing.Size(60, 21);
            this._comboVersionTml.TabIndex = 3;
            this._tips.SetToolTip(this._comboVersionTml, "Select tModLoader version.");
            this._comboVersionTml.SelectedIndexChanged += new System.EventHandler(this._comboVersionTml_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "T&ML:";
            // 
            // _checkOverwriteClient
            // 
            this._checkOverwriteClient.AutoSize = true;
            this._checkOverwriteClient.Location = new System.Drawing.Point(224, 11);
            this._checkOverwriteClient.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this._checkOverwriteClient.Name = "_checkOverwriteClient";
            this._checkOverwriteClient.Size = new System.Drawing.Size(100, 17);
            this._checkOverwriteClient.TabIndex = 4;
            this._checkOverwriteClient.Text = "&Overwrite Client";
            this._tips.SetToolTip(this._checkOverwriteClient, "Should Terraria.exe be overwritten or generate tModLoader.exe.");
            this._checkOverwriteClient.UseVisualStyleBackColor = true;
            // 
            // _checkOverwriteServer
            // 
            this._checkOverwriteServer.AutoSize = true;
            this._checkOverwriteServer.Location = new System.Drawing.Point(224, 27);
            this._checkOverwriteServer.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this._checkOverwriteServer.Name = "_checkOverwriteServer";
            this._checkOverwriteServer.Size = new System.Drawing.Size(105, 17);
            this._checkOverwriteServer.TabIndex = 5;
            this._checkOverwriteServer.Text = "O&verwrite Server";
            this._tips.SetToolTip(this._checkOverwriteServer, "Should TerrariaServer.exe be overwritten or generate tModLoaderServer.exe.");
            this._checkOverwriteServer.UseVisualStyleBackColor = true;
            // 
            // _groupMisc
            // 
            this._groupMisc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._groupMisc.Controls.Add(this.label1);
            this._groupMisc.Controls.Add(this._checkOverwriteServer);
            this._groupMisc.Controls.Add(this._comboVersionTer);
            this._groupMisc.Controls.Add(this._checkOverwriteClient);
            this._groupMisc.Controls.Add(this._comboVersionTml);
            this._groupMisc.Controls.Add(this.label2);
            this._groupMisc.Location = new System.Drawing.Point(12, 68);
            this._groupMisc.Name = "_groupMisc";
            this._groupMisc.Size = new System.Drawing.Size(360, 47);
            this._groupMisc.TabIndex = 2;
            this._groupMisc.TabStop = false;
            // 
            // _checkInstallDependencies
            // 
            this._checkInstallDependencies.AutoSize = true;
            this._checkInstallDependencies.Checked = true;
            this._checkInstallDependencies.CheckState = System.Windows.Forms.CheckState.Checked;
            this._checkInstallDependencies.Location = new System.Drawing.Point(188, 11);
            this._checkInstallDependencies.Name = "_checkInstallDependencies";
            this._checkInstallDependencies.Size = new System.Drawing.Size(125, 17);
            this._checkInstallDependencies.TabIndex = 2;
            this._checkInstallDependencies.Text = "Install &Dependencies";
            this._tips.SetToolTip(this._checkInstallDependencies, "Should tModLoader dependency files be installed.");
            this._checkInstallDependencies.UseVisualStyleBackColor = true;
            this._checkInstallDependencies.CheckedChanged += new System.EventHandler(this._checkInstallDependencies_CheckedChanged);
            // 
            // _checkInstallServer
            // 
            this._checkInstallServer.AutoSize = true;
            this._checkInstallServer.Checked = true;
            this._checkInstallServer.CheckState = System.Windows.Forms.CheckState.Checked;
            this._checkInstallServer.Location = new System.Drawing.Point(95, 11);
            this._checkInstallServer.Name = "_checkInstallServer";
            this._checkInstallServer.Size = new System.Drawing.Size(87, 17);
            this._checkInstallServer.TabIndex = 1;
            this._checkInstallServer.Text = "Install &Server";
            this._tips.SetToolTip(this._checkInstallServer, "Should tModLoader server patch be installed.");
            this._checkInstallServer.UseVisualStyleBackColor = true;
            this._checkInstallServer.CheckedChanged += new System.EventHandler(this._checkInstallServer_CheckedChanged);
            // 
            // _checkInstallClient
            // 
            this._checkInstallClient.AutoSize = true;
            this._checkInstallClient.Checked = true;
            this._checkInstallClient.CheckState = System.Windows.Forms.CheckState.Checked;
            this._checkInstallClient.Location = new System.Drawing.Point(7, 11);
            this._checkInstallClient.Name = "_checkInstallClient";
            this._checkInstallClient.Size = new System.Drawing.Size(82, 17);
            this._checkInstallClient.TabIndex = 0;
            this._checkInstallClient.Text = "Install &Client";
            this._tips.SetToolTip(this._checkInstallClient, "Should tModLoader client patch be installed.");
            this._checkInstallClient.UseVisualStyleBackColor = true;
            this._checkInstallClient.CheckedChanged += new System.EventHandler(this._checkInstallClient_CheckedChanged);
            // 
            // _groupInstall
            // 
            this._groupInstall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._groupInstall.Controls.Add(this._buttonInstall);
            this._groupInstall.Location = new System.Drawing.Point(12, 130);
            this._groupInstall.Name = "_groupInstall";
            this._groupInstall.Size = new System.Drawing.Size(360, 40);
            this._groupInstall.TabIndex = 4;
            this._groupInstall.TabStop = false;
            // 
            // _picDirPathStatus
            // 
            this._picDirPathStatus.Location = new System.Drawing.Point(7, 13);
            this._picDirPathStatus.Margin = new System.Windows.Forms.Padding(0);
            this._picDirPathStatus.Name = "_picDirPathStatus";
            this._picDirPathStatus.Size = new System.Drawing.Size(20, 20);
            this._picDirPathStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._picDirPathStatus.TabIndex = 12;
            this._picDirPathStatus.TabStop = false;
            // 
            // _buttonBrowse
            // 
            this._buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._buttonBrowse.BackgroundImage = global::TmlGogPatcher.Properties.Resources.Browse;
            this._buttonBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._buttonBrowse.Location = new System.Drawing.Point(306, 11);
            this._buttonBrowse.Margin = new System.Windows.Forms.Padding(0);
            this._buttonBrowse.Name = "_buttonBrowse";
            this._buttonBrowse.Size = new System.Drawing.Size(24, 24);
            this._buttonBrowse.TabIndex = 1;
            this._tips.SetToolTip(this._buttonBrowse, "Browse file system for Terraria directory.");
            this._buttonBrowse.UseVisualStyleBackColor = true;
            this._buttonBrowse.Click += new System.EventHandler(this._buttonBrowse_Click);
            // 
            // _buttonOpen
            // 
            this._buttonOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._buttonOpen.BackgroundImage = global::TmlGogPatcher.Properties.Resources.Open;
            this._buttonOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._buttonOpen.Location = new System.Drawing.Point(330, 11);
            this._buttonOpen.Margin = new System.Windows.Forms.Padding(0);
            this._buttonOpen.Name = "_buttonOpen";
            this._buttonOpen.Size = new System.Drawing.Size(24, 24);
            this._buttonOpen.TabIndex = 2;
            this._tips.SetToolTip(this._buttonOpen, "Open selected directory.");
            this._buttonOpen.UseVisualStyleBackColor = true;
            this._buttonOpen.Click += new System.EventHandler(this._buttonOpen_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._checkInstallDependencies);
            this.groupBox1.Controls.Add(this._checkInstallClient);
            this.groupBox1.Controls.Add(this._checkInstallServer);
            this.groupBox1.Location = new System.Drawing.Point(12, 107);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 31);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this._picDirPathStatus);
            this.groupBox2.Controls.Add(this._textPath);
            this.groupBox2.Controls.Add(this._buttonBrowse);
            this.groupBox2.Controls.Add(this._buttonOpen);
            this.groupBox2.Location = new System.Drawing.Point(12, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 40);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // _stripTools
            // 
            this._stripTools.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._stripTools.ImageScalingSize = new System.Drawing.Size(30, 30);
            this._stripTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolAbout,
            this._separator3,
            this._toolVisit,
            this._toolFolder,
            this._toolViewLog,
            this._toolPatches});
            this._stripTools.Location = new System.Drawing.Point(0, 0);
            this._stripTools.Name = "_stripTools";
            this._stripTools.Size = new System.Drawing.Size(384, 37);
            this._stripTools.TabIndex = 0;
            this._stripTools.Text = "_stripTools";
            // 
            // _toolAbout
            // 
            this._toolAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._toolAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolAbout.Image = global::TmlGogPatcher.Properties.Resources.About;
            this._toolAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolAbout.Name = "_toolAbout";
            this._toolAbout.Size = new System.Drawing.Size(34, 34);
            this._toolAbout.Text = "View Application About Dialog...";
            this._toolAbout.Click += new System.EventHandler(this._doAbout_Click);
            // 
            // _separator3
            // 
            this._separator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._separator3.Name = "_separator3";
            this._separator3.Size = new System.Drawing.Size(6, 37);
            // 
            // _toolVisit
            // 
            this._toolVisit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._toolVisit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolVisit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolVisitPatcher,
            this._toolVisitTerraria,
            this._toolVisitTml});
            this._toolVisit.Image = global::TmlGogPatcher.Properties.Resources.Internet;
            this._toolVisit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolVisit.Name = "_toolVisit";
            this._toolVisit.Size = new System.Drawing.Size(43, 34);
            this._toolVisit.Text = "Visit Webpages";
            // 
            // _toolVisitPatcher
            // 
            this._toolVisitPatcher.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolVisitPatcherWebsite,
            this._toolVisitPatcherThread,
            this._toolVisitPatcherGitlab,
            this._toolVisitPatcherWiki});
            this._toolVisitPatcher.Image = global::TmlGogPatcher.Properties.Resources.VisitPagePatcher;
            this._toolVisitPatcher.Name = "_toolVisitPatcher";
            this._toolVisitPatcher.Size = new System.Drawing.Size(222, 22);
            this._toolVisitPatcher.Text = "Visit TMLGOGPatcher Pages";
            // 
            // _toolVisitPatcherWebsite
            // 
            this._toolVisitPatcherWebsite.Enabled = false;
            this._toolVisitPatcherWebsite.Name = "_toolVisitPatcherWebsite";
            this._toolVisitPatcherWebsite.Size = new System.Drawing.Size(242, 22);
            this._toolVisitPatcherWebsite.Text = "Visit TMLGOGPatcher &Website...";
            this._toolVisitPatcherWebsite.Url = null;
            // 
            // _toolVisitPatcherThread
            // 
            this._toolVisitPatcherThread.Enabled = false;
            this._toolVisitPatcherThread.Name = "_toolVisitPatcherThread";
            this._toolVisitPatcherThread.Size = new System.Drawing.Size(242, 22);
            this._toolVisitPatcherThread.Text = "Visit TMLGOGPatcher &Thread...";
            this._toolVisitPatcherThread.Url = null;
            // 
            // _toolVisitPatcherGitlab
            // 
            this._toolVisitPatcherGitlab.Enabled = false;
            this._toolVisitPatcherGitlab.Name = "_toolVisitPatcherGitlab";
            this._toolVisitPatcherGitlab.Size = new System.Drawing.Size(242, 22);
            this._toolVisitPatcherGitlab.Text = "Visit TMLGOGPatcher &Gitlab...";
            this._toolVisitPatcherGitlab.Url = null;
            // 
            // _toolVisitPatcherWiki
            // 
            this._toolVisitPatcherWiki.Enabled = false;
            this._toolVisitPatcherWiki.Name = "_toolVisitPatcherWiki";
            this._toolVisitPatcherWiki.Size = new System.Drawing.Size(242, 22);
            this._toolVisitPatcherWiki.Text = "Visit TMLGOGPatcher &Wiki...";
            this._toolVisitPatcherWiki.Url = null;
            // 
            // _toolVisitTerraria
            // 
            this._toolVisitTerraria.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolVisitTerrariaWebsite,
            this._toolVisitTerrariaForum,
            this._toolVisitTerrariaForumRules,
            this._toolVisitTerrariaModRules});
            this._toolVisitTerraria.Image = global::TmlGogPatcher.Properties.Resources.VisitPageTerraria;
            this._toolVisitTerraria.Name = "_toolVisitTerraria";
            this._toolVisitTerraria.Size = new System.Drawing.Size(222, 22);
            this._toolVisitTerraria.Text = "Visit Terraria Pages";
            // 
            // _toolVisitTerrariaWebsite
            // 
            this._toolVisitTerrariaWebsite.Enabled = false;
            this._toolVisitTerrariaWebsite.Name = "_toolVisitTerrariaWebsite";
            this._toolVisitTerrariaWebsite.Size = new System.Drawing.Size(252, 22);
            this._toolVisitTerrariaWebsite.Text = "Visit Terraria &Website...";
            this._toolVisitTerrariaWebsite.Url = null;
            // 
            // _toolVisitTerrariaForum
            // 
            this._toolVisitTerrariaForum.Enabled = false;
            this._toolVisitTerrariaForum.Name = "_toolVisitTerrariaForum";
            this._toolVisitTerrariaForum.Size = new System.Drawing.Size(252, 22);
            this._toolVisitTerrariaForum.Text = "Visit Terraria &Community Forum...";
            this._toolVisitTerrariaForum.Url = null;
            // 
            // _toolVisitTerrariaForumRules
            // 
            this._toolVisitTerrariaForumRules.Enabled = false;
            this._toolVisitTerrariaForumRules.Name = "_toolVisitTerrariaForumRules";
            this._toolVisitTerrariaForumRules.Size = new System.Drawing.Size(252, 22);
            this._toolVisitTerrariaForumRules.Text = "Visit Terraria &Forum Rules...";
            this._toolVisitTerrariaForumRules.Url = null;
            // 
            // _toolVisitTerrariaModRules
            // 
            this._toolVisitTerrariaModRules.Enabled = false;
            this._toolVisitTerrariaModRules.Name = "_toolVisitTerrariaModRules";
            this._toolVisitTerrariaModRules.Size = new System.Drawing.Size(252, 22);
            this._toolVisitTerrariaModRules.Text = "Visit Terraria &Modding Rules...";
            this._toolVisitTerrariaModRules.Url = null;
            // 
            // _toolVisitTml
            // 
            this._toolVisitTml.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolVisitTmlThread,
            this._toolVisitTmlGithub,
            this._toolVisitTmlWiki});
            this._toolVisitTml.Image = global::TmlGogPatcher.Properties.Resources.VisitPageTml;
            this._toolVisitTml.Name = "_toolVisitTml";
            this._toolVisitTml.Size = new System.Drawing.Size(222, 22);
            this._toolVisitTml.Text = "Visit tModLoader Pages";
            // 
            // _toolVisitTmlThread
            // 
            this._toolVisitTmlThread.Enabled = false;
            this._toolVisitTmlThread.Name = "_toolVisitTmlThread";
            this._toolVisitTmlThread.Size = new System.Drawing.Size(213, 22);
            this._toolVisitTmlThread.Text = "Visit tModLoader &Thread...";
            this._toolVisitTmlThread.Url = null;
            // 
            // _toolVisitTmlGithub
            // 
            this._toolVisitTmlGithub.Enabled = false;
            this._toolVisitTmlGithub.Name = "_toolVisitTmlGithub";
            this._toolVisitTmlGithub.Size = new System.Drawing.Size(213, 22);
            this._toolVisitTmlGithub.Text = "Visit tModLoader &Github...";
            this._toolVisitTmlGithub.Url = null;
            // 
            // _toolVisitTmlWiki
            // 
            this._toolVisitTmlWiki.Enabled = false;
            this._toolVisitTmlWiki.Name = "_toolVisitTmlWiki";
            this._toolVisitTmlWiki.Size = new System.Drawing.Size(213, 22);
            this._toolVisitTmlWiki.Text = "Visit tModLoader &Wiki...";
            this._toolVisitTmlWiki.Url = null;
            // 
            // _toolFolder
            // 
            this._toolFolder.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._toolFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolFolder.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpenFoldersPatcher,
            this._toolOpenFoldersTerraria,
            this._toolOpenFoldersTml});
            this._toolFolder.Image = global::TmlGogPatcher.Properties.Resources.Open;
            this._toolFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolFolder.Name = "_toolFolder";
            this._toolFolder.Size = new System.Drawing.Size(43, 34);
            this._toolFolder.Text = "Open Folders";
            // 
            // _toolOpenFoldersPatcher
            // 
            this._toolOpenFoldersPatcher.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpenFolderPatcher,
            this._toolOpenFolderPatcherLogs,
            this._toolOpenFolderPatcherPatches});
            this._toolOpenFoldersPatcher.Image = global::TmlGogPatcher.Properties.Resources.OpenFolderPatcher;
            this._toolOpenFoldersPatcher.Name = "_toolOpenFoldersPatcher";
            this._toolOpenFoldersPatcher.Size = new System.Drawing.Size(236, 22);
            this._toolOpenFoldersPatcher.Text = "Open TMLGOGPatcher Folders";
            // 
            // _toolOpenFolderPatcher
            // 
            this._toolOpenFolderPatcher.Enabled = false;
            this._toolOpenFolderPatcher.Name = "_toolOpenFolderPatcher";
            this._toolOpenFolderPatcher.Path = null;
            this._toolOpenFolderPatcher.Size = new System.Drawing.Size(284, 22);
            this._toolOpenFolderPatcher.Text = "Open TMLGOGPatcher &Folder...";
            // 
            // _toolOpenFolderPatcherLogs
            // 
            this._toolOpenFolderPatcherLogs.Enabled = false;
            this._toolOpenFolderPatcherLogs.Name = "_toolOpenFolderPatcherLogs";
            this._toolOpenFolderPatcherLogs.Path = null;
            this._toolOpenFolderPatcherLogs.Size = new System.Drawing.Size(284, 22);
            this._toolOpenFolderPatcherLogs.Text = "Open TMLGOGPatcher &Logs Folder...";
            // 
            // _toolOpenFolderPatcherPatches
            // 
            this._toolOpenFolderPatcherPatches.Enabled = false;
            this._toolOpenFolderPatcherPatches.Name = "_toolOpenFolderPatcherPatches";
            this._toolOpenFolderPatcherPatches.Path = null;
            this._toolOpenFolderPatcherPatches.Size = new System.Drawing.Size(284, 22);
            this._toolOpenFolderPatcherPatches.Text = "Open TMLGOGPatcher &Patches Folder...";
            // 
            // _toolOpenFoldersTerraria
            // 
            this._toolOpenFoldersTerraria.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpenFolderTerraria,
            this._toolOpenFolderTerrariaCaptures,
            this._toolOpenFolderTerrariaLogs,
            this._toolOpenFolderTerrariaPlayers,
            this._toolOpenFolderTerrariaWorlds});
            this._toolOpenFoldersTerraria.Image = global::TmlGogPatcher.Properties.Resources.OpenFolderTerraria;
            this._toolOpenFoldersTerraria.Name = "_toolOpenFoldersTerraria";
            this._toolOpenFoldersTerraria.Size = new System.Drawing.Size(236, 22);
            this._toolOpenFoldersTerraria.Text = "Open Terraria Folders";
            // 
            // _toolOpenFolderTerraria
            // 
            this._toolOpenFolderTerraria.Enabled = false;
            this._toolOpenFolderTerraria.Name = "_toolOpenFolderTerraria";
            this._toolOpenFolderTerraria.Path = null;
            this._toolOpenFolderTerraria.Size = new System.Drawing.Size(240, 22);
            this._toolOpenFolderTerraria.Text = "Open Terraria &Folder...";
            // 
            // _toolOpenFolderTerrariaCaptures
            // 
            this._toolOpenFolderTerrariaCaptures.Enabled = false;
            this._toolOpenFolderTerrariaCaptures.Name = "_toolOpenFolderTerrariaCaptures";
            this._toolOpenFolderTerrariaCaptures.Path = null;
            this._toolOpenFolderTerrariaCaptures.Size = new System.Drawing.Size(240, 22);
            this._toolOpenFolderTerrariaCaptures.Text = "Open Terraria &Captures Folder...";
            // 
            // _toolOpenFolderTerrariaLogs
            // 
            this._toolOpenFolderTerrariaLogs.Enabled = false;
            this._toolOpenFolderTerrariaLogs.Name = "_toolOpenFolderTerrariaLogs";
            this._toolOpenFolderTerrariaLogs.Path = null;
            this._toolOpenFolderTerrariaLogs.Size = new System.Drawing.Size(240, 22);
            this._toolOpenFolderTerrariaLogs.Text = "Open Terraria &Logs Folder...";
            // 
            // _toolOpenFolderTerrariaPlayers
            // 
            this._toolOpenFolderTerrariaPlayers.Enabled = false;
            this._toolOpenFolderTerrariaPlayers.Name = "_toolOpenFolderTerrariaPlayers";
            this._toolOpenFolderTerrariaPlayers.Path = null;
            this._toolOpenFolderTerrariaPlayers.Size = new System.Drawing.Size(240, 22);
            this._toolOpenFolderTerrariaPlayers.Text = "Open Terraria &Players Folder...";
            // 
            // _toolOpenFolderTerrariaWorlds
            // 
            this._toolOpenFolderTerrariaWorlds.Enabled = false;
            this._toolOpenFolderTerrariaWorlds.Name = "_toolOpenFolderTerrariaWorlds";
            this._toolOpenFolderTerrariaWorlds.Path = null;
            this._toolOpenFolderTerrariaWorlds.Size = new System.Drawing.Size(240, 22);
            this._toolOpenFolderTerrariaWorlds.Text = "Open Terraria &Worlds Folder...";
            // 
            // _toolOpenFoldersTml
            // 
            this._toolOpenFoldersTml.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolOpenFolderTml,
            this._toolOpenFolderTmlCaptures,
            this._toolOpenFolderTmlLogs,
            this._toolOpenFolderTmlMods,
            this._toolOpenFolderTmlSources,
            this._toolOpenFolderTmlPlayers,
            this._toolOpenFolderTmlWorlds});
            this._toolOpenFoldersTml.Image = global::TmlGogPatcher.Properties.Resources.OpenFolderTml;
            this._toolOpenFoldersTml.Name = "_toolOpenFoldersTml";
            this._toolOpenFoldersTml.Size = new System.Drawing.Size(236, 22);
            this._toolOpenFoldersTml.Text = "Open tModLoader Folders";
            // 
            // _toolOpenFolderTml
            // 
            this._toolOpenFolderTml.Enabled = false;
            this._toolOpenFolderTml.Name = "_toolOpenFolderTml";
            this._toolOpenFolderTml.Path = null;
            this._toolOpenFolderTml.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTml.Text = "Open tModLoader &Folder...";
            // 
            // _toolOpenFolderTmlCaptures
            // 
            this._toolOpenFolderTmlCaptures.Enabled = false;
            this._toolOpenFolderTmlCaptures.Name = "_toolOpenFolderTmlCaptures";
            this._toolOpenFolderTmlCaptures.Path = null;
            this._toolOpenFolderTmlCaptures.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlCaptures.Text = "Open tModLoader &Captures Folder...";
            // 
            // _toolOpenFolderTmlLogs
            // 
            this._toolOpenFolderTmlLogs.Enabled = false;
            this._toolOpenFolderTmlLogs.Name = "_toolOpenFolderTmlLogs";
            this._toolOpenFolderTmlLogs.Path = null;
            this._toolOpenFolderTmlLogs.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlLogs.Text = "Open tModLoader &Logs Folder...";
            // 
            // _toolOpenFolderTmlMods
            // 
            this._toolOpenFolderTmlMods.Enabled = false;
            this._toolOpenFolderTmlMods.Name = "_toolOpenFolderTmlMods";
            this._toolOpenFolderTmlMods.Path = null;
            this._toolOpenFolderTmlMods.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlMods.Text = "Open tModLoader &Mods Folder...";
            // 
            // _toolOpenFolderTmlSources
            // 
            this._toolOpenFolderTmlSources.Enabled = false;
            this._toolOpenFolderTmlSources.Name = "_toolOpenFolderTmlSources";
            this._toolOpenFolderTmlSources.Path = null;
            this._toolOpenFolderTmlSources.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlSources.Text = "Open tModLoader &Sources Folder...";
            // 
            // _toolOpenFolderTmlPlayers
            // 
            this._toolOpenFolderTmlPlayers.Enabled = false;
            this._toolOpenFolderTmlPlayers.Name = "_toolOpenFolderTmlPlayers";
            this._toolOpenFolderTmlPlayers.Path = null;
            this._toolOpenFolderTmlPlayers.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlPlayers.Text = "Open tModLoader &Players Folder...";
            // 
            // _toolOpenFolderTmlWorlds
            // 
            this._toolOpenFolderTmlWorlds.Enabled = false;
            this._toolOpenFolderTmlWorlds.Name = "_toolOpenFolderTmlWorlds";
            this._toolOpenFolderTmlWorlds.Path = null;
            this._toolOpenFolderTmlWorlds.Size = new System.Drawing.Size(266, 22);
            this._toolOpenFolderTmlWorlds.Text = "Open tModLoader &Worlds Folder...";
            // 
            // _toolViewLog
            // 
            this._toolViewLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolViewLog.Image = global::TmlGogPatcher.Properties.Resources.Log;
            this._toolViewLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolViewLog.Name = "_toolViewLog";
            this._toolViewLog.Size = new System.Drawing.Size(34, 34);
            this._toolViewLog.Text = "View Application Log File...";
            this._toolViewLog.Click += new System.EventHandler(this._doViewLog_Click);
            // 
            // _toolPatches
            // 
            this._toolPatches.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolPatches.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolPatchDownload,
            this._toolPatchList});
            this._toolPatches.Image = global::TmlGogPatcher.Properties.Resources.Patch;
            this._toolPatches.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolPatches.Name = "_toolPatches";
            this._toolPatches.Size = new System.Drawing.Size(43, 34);
            this._toolPatches.Text = "Patch Tools";
            // 
            // _toolPatchDownload
            // 
            this._toolPatchDownload.Name = "_toolPatchDownload";
            this._toolPatchDownload.Size = new System.Drawing.Size(170, 22);
            this._toolPatchDownload.Text = "Download Patch...";
            this._toolPatchDownload.Click += new System.EventHandler(this._toolPatchDownload_Click);
            // 
            // _toolPatchList
            // 
            this._toolPatchList.Name = "_toolPatchList";
            this._toolPatchList.Size = new System.Drawing.Size(170, 22);
            this._toolPatchList.Text = "List Patches...";
            this._toolPatchList.Click += new System.EventHandler(this._toolPatchList_Click);
            // 
            // _stripStatus
            // 
            this._stripStatus.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._stripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolRefresh,
            this._status,
            this._progress});
            this._stripStatus.Location = new System.Drawing.Point(0, 282);
            this._stripStatus.Name = "_stripStatus";
            this._stripStatus.ShowItemToolTips = true;
            this._stripStatus.Size = new System.Drawing.Size(384, 30);
            this._stripStatus.TabIndex = 6;
            this._stripStatus.Text = "statusStrip1";
            // 
            // _toolRefresh
            // 
            this._toolRefresh.AutoToolTip = false;
            this._toolRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._toolRefresh.Image = global::TmlGogPatcher.Properties.Resources.Refresh_x16;
            this._toolRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._toolRefresh.Name = "_toolRefresh";
            this._toolRefresh.ShowDropDownArrow = false;
            this._toolRefresh.Size = new System.Drawing.Size(28, 28);
            this._toolRefresh.Text = "Refresh";
            this._toolRefresh.ToolTipText = "In rare cases the interface will not automatically update.\r\nIn those cases, click" +
    " this button to force the interface to update.";
            this._toolRefresh.Click += new System.EventHandler(this._doRefresh_Click);
            // 
            // _status
            // 
            this._status.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._status.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(341, 25);
            this._status.Spring = true;
            this._status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _progress
            // 
            this._progress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._progress.Margin = new System.Windows.Forms.Padding(1);
            this._progress.MarqueeAnimationSpeed = 25;
            this._progress.Name = "_progress";
            this._progress.Size = new System.Drawing.Size(100, 28);
            this._progress.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 312);
            this.Controls.Add(this._stripStatus);
            this.Controls.Add(this._stripTools);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._groupMisc);
            this.Controls.Add(this._textLog);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._groupInstall);
            this.MinimumSize = new System.Drawing.Size(398, 344);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this._groupMisc.ResumeLayout(false);
            this._groupMisc.PerformLayout();
            this._groupInstall.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._picDirPathStatus)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._stripTools.ResumeLayout(false);
            this._stripTools.PerformLayout();
            this._stripStatus.ResumeLayout(false);
            this._stripStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _buttonOpen;
        private System.Windows.Forms.TextBox _textPath;
        private System.Windows.Forms.TextBox _textLog;
        private System.Windows.Forms.Button _buttonInstall;
        private System.Windows.Forms.ComboBox _comboVersionTer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _comboVersionTml;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox _checkOverwriteClient;
        private System.Windows.Forms.CheckBox _checkOverwriteServer;
        private System.Windows.Forms.GroupBox _groupMisc;
        private System.Windows.Forms.Button _buttonBrowse;
        private System.Windows.Forms.PictureBox _picDirPathStatus;
        private System.Windows.Forms.GroupBox _groupInstall;
        private System.Windows.Forms.CheckBox _checkInstallDependencies;
        private System.Windows.Forms.CheckBox _checkInstallServer;
        private System.Windows.Forms.CheckBox _checkInstallClient;
        private System.Windows.Forms.ToolTip _tips;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStrip _stripTools;
        private System.Windows.Forms.ToolStripButton _toolAbout;
        private System.Windows.Forms.ToolStripSeparator _separator3;
        private System.Windows.Forms.ToolStripButton _toolViewLog;
        private System.Windows.Forms.StatusStrip _stripStatus;
        private System.Windows.Forms.ToolStripStatusLabel _status;
        private System.Windows.Forms.ToolStripProgressBar _progress;
        private System.Windows.Forms.ToolStripDropDownButton _toolVisit;
        private System.Windows.Forms.ToolStripMenuItem _toolVisitTerraria;
        private Controls.ToolStripUrlMenuItem _toolVisitTerrariaWebsite;
        private Controls.ToolStripUrlMenuItem _toolVisitTerrariaForum;
        private Controls.ToolStripUrlMenuItem _toolVisitTerrariaForumRules;
        private Controls.ToolStripUrlMenuItem _toolVisitTerrariaModRules;
        private System.Windows.Forms.ToolStripMenuItem _toolVisitTml;
        private Controls.ToolStripUrlMenuItem _toolVisitTmlThread;
        private Controls.ToolStripUrlMenuItem _toolVisitTmlGithub;
        private Controls.ToolStripUrlMenuItem _toolVisitTmlWiki;
        private System.Windows.Forms.ToolStripDropDownButton _toolFolder;
        private System.Windows.Forms.ToolStripMenuItem _toolOpenFoldersTerraria;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTerraria;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTerrariaCaptures;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTerrariaLogs;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTerrariaPlayers;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTerrariaWorlds;
        private System.Windows.Forms.ToolStripMenuItem _toolOpenFoldersTml;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTml;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlCaptures;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlLogs;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlMods;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlSources;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlPlayers;
        private Controls.ToolStripPathMenuItem _toolOpenFolderTmlWorlds;
        private System.Windows.Forms.ToolStripDropDownButton _toolRefresh;
        private System.Windows.Forms.ToolStripMenuItem _toolVisitPatcher;
        private Controls.ToolStripUrlMenuItem _toolVisitPatcherWebsite;
        private Controls.ToolStripUrlMenuItem _toolVisitPatcherThread;
        private Controls.ToolStripUrlMenuItem _toolVisitPatcherGitlab;
        private Controls.ToolStripUrlMenuItem _toolVisitPatcherWiki;
        private System.Windows.Forms.ToolStripMenuItem _toolOpenFoldersPatcher;
        private Controls.ToolStripPathMenuItem _toolOpenFolderPatcher;
        private Controls.ToolStripPathMenuItem _toolOpenFolderPatcherLogs;
        private Controls.ToolStripPathMenuItem _toolOpenFolderPatcherPatches;
        private System.Windows.Forms.ToolStripDropDownButton _toolPatches;
        private System.Windows.Forms.ToolStripMenuItem _toolPatchDownload;
        private System.Windows.Forms.ToolStripMenuItem _toolPatchList;
    }
}

