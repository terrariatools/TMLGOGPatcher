﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

using System;
using System.Linq;

namespace Common.Logging.Loggers
{
    /// <summary>
    /// Abstract class representing a standard logging object.
    /// </summary>
    public abstract class ALogger : ILogger
    {
        /// <summary>
        /// Get or set the new line sequence.
        /// </summary>
        public virtual string NewLine { get { return this._newLine; } set { this._newLine = value; } }
        /// <summary>
        /// Backing variable for NewLine property.
        /// </summary>
        protected string _newLine = Environment.NewLine;

        /// <summary>
        /// Get or set format used to compose a single log record.
        /// </summary>
        public virtual string RecordFormat { get { return this._recordFormat; } set { this._recordFormat = value; } }
        /// <summary>
        /// Backing variable for RecordFormat property.
        /// </summary>
        protected string _recordFormat = "{0}: {1}";

        /// <summary>
        /// Get or set level used to filter messages.
        /// </summary>
        public LogLevelEnums LevelFilter { get { return this._levelFilter; } set { this._levelFilter = value; } }
        /// <summary>
        /// Backing variable for LevelFilter property.
        /// </summary>
        protected LogLevelEnums _levelFilter = LogLevelEnums.All;

        /// <summary>
        /// Check is a level will be logged.
        /// </summary>
        /// <param name="level">Level to check.</param>
        /// <returns>True if the level will be logged.</returns>
        public virtual bool IsLevelLoggable(LogLevelEnums level)
        {
            if (this.LevelFilter == LogLevelEnums.None) { return false; }
            if (level == LogLevelEnums.None) { return false; }
            return this.LevelFilter.HasFlag(level);
        }

        /// <summary>
        /// Add to the level filter.
        /// </summary>
        /// <param name="level">Level to add to the filter.</param>
        public virtual void AddLevelFilter(LogLevelEnums level)
        {
            if (level == LogLevelEnums.None) { return; }
            this.LevelFilter |= level;
        }

        /// <summary>
        /// Remove from the level filter.
        /// </summary>
        /// <param name="level">Level to remove from the filter.</param>
        public virtual void RemoveLevelFilter(LogLevelEnums level)
        {
            if (level == LogLevelEnums.None) { return; }
            this.LevelFilter &= ~level;
        }

        /// <summary>
        /// Form a log record with the default format and the given level and value.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="value">Text of the log record.</param>
        /// <returns>Formated log record.</returns>
        public virtual string FormRecord(LogLevelEnums level, string value)
        {
            return string.Format(this.RecordFormat, Enum.GetName(typeof(LogLevelEnums), level), value);
        }

        /// <summary>
        /// Form a log record with the given format, level, and values.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        /// <returns>Formated log record.</returns>
        public virtual string FormRecord(LogLevelEnums level, string format, params object[] values)
        {
            return string.Format(this.RecordFormat, Enum.GetName(typeof(LogLevelEnums), level), string.Format(format, values));
        }

        /// <summary>
        /// Log an unfiltered string of text. When overriding in derived classes,
        /// this method should accept empty string as a valid argument, append a
        /// new line to the end of the given text, and should not modify the given
        /// text in any other manner not required to transfer the text to the
        /// target of the output.
        /// All other methods should ultimately pass their content through this
        /// method to perform the actual logging operation.
        /// </summary>
        /// <param name="text">String of text to log.</param>
        public abstract void Log(string text);

        /// <summary>
        /// Log one or more blank lines of text.
        /// </summary>
        /// <param name="count">Number of blank lines to log.</param>
        public virtual void Log(int count = 1)
        {
            if (count < 1) { throw new ArgumentOutOfRangeException("count", count, "Argument must be greater than zero."); }
            else if (count == 1) { this.Log(string.Empty); }
            else { this.Log(string.Concat(Enumerable.Repeat(this.NewLine, count - 1))); }
        }

        /// <summary>
        /// Format and log an unfiltered string of text.
        /// </summary>
        /// <param name="format">Format of log text.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void Log(string format, params object[] values)
        {
            this.Log(string.Format(format, values));
        }

        /// <summary>
        /// Log a record with the default format and the given level and value.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="value">Text of the log record.</param>
        public virtual void Log(LogLevelEnums level, string value)
        {
            if (!this.IsLevelLoggable(level)) { return; }
            this.Log(this.FormRecord(level, value));
        }

        /// <summary>
        /// Log a record with the given format, level, and values.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void Log(LogLevelEnums level, string format, params object[] values)
        {
            if (!this.IsLevelLoggable(level)) { return; }
            this.Log(this.FormRecord(level, format, values));
        }

        /// <summary>
        /// Log an error record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        public virtual void LogError(string value) { this.Log(LogLevelEnums.Error, value); }

        /// <summary>
        /// Log an error record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void LogError(string format, params object[] values) { this.Log(LogLevelEnums.Error, format, values); }

        /// <summary>
        /// Log a warning record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        public virtual void LogWarning(string value) { this.Log(LogLevelEnums.Warning, value); }

        /// <summary>
        /// Log a warning record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void LogWarning(string format, params object[] values) { this.Log(LogLevelEnums.Warning, format, values); }

        /// <summary>
        /// Log an info record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        public virtual void LogInfo(string value) { this.Log(LogLevelEnums.Info, value); }

        /// <summary>
        /// Log an info record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void LogInfo(string format, params object[] values) { this.Log(LogLevelEnums.Info, format, values); }

        /// <summary>
        /// Log a debug record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        public virtual void LogDebug(string value) { this.Log(LogLevelEnums.Debug, value); }

        /// <summary>
        /// Log a debug record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public virtual void LogDebug(string format, params object[] values) { this.Log(LogLevelEnums.Debug, format, values); }
    }
}
