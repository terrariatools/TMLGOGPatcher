﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

using System;
using System.IO;

namespace Common.Logging.Loggers
{
    /// <summary>
    /// Class representing a file based logging object.
    /// </summary>
    public class FileLogger : ALogger
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileLogger()
        {
        }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="filePath">Path to target file.</param>
        public FileLogger(string filePath)
        {
            this.Target = new FileInfo(filePath);
        }

        /// <summary>
        /// Get or set the target file used for logging.
        /// </summary>
        public FileInfo Target { get { return this._target; } set { this._target = value; } }
        /// <summary>
        /// Backing variable for Target property.
        /// </summary>
        private FileInfo _target = null;

        /// <summary>
        /// Log an unfiltered string of text.
        /// </summary>
        /// <param name="text">String of text to log.</param>
        public override void Log(string text)
        {
            if (this.Target == null) { throw new InvalidOperationException("FileLogger Target must be non-null before call Log()."); }
            File.AppendAllText(this.Target.FullName, text + this.NewLine);
        }
    }
}
