﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

using System;

namespace Common.Logging.Loggers
{
    /// <summary>
    /// Class representing a delegate based logging object.
    /// </summary>
    public class DelegateLogger : ALogger
    {
        /// <summary>
        /// Delegate representing a method called to log a formatted record.
        /// </summary>
        /// <param name="record">Fully formatted record.</param>
        public delegate void LogHandlerDelegate(string record);

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DelegateLogger()
        {
        }

        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="handler">Handler used to log records.</param>
        public DelegateLogger(LogHandlerDelegate handler)
        {
            this.Handler = handler;
        }

        /// <summary>
        /// Get or set the handler representing the method used
        /// to log a formatted record.
        /// </summary>
        public LogHandlerDelegate Handler { get { return this._handler; } set { this._handler = value; } }
        /// <summary>
        /// Backing variable for Handler property.
        /// </summary>
        private LogHandlerDelegate _handler = null;

        /// <summary>
        /// Log an unfiltered string of text.
        /// </summary>
        /// <param name="text">String of text to log.</param>
        public override void Log(string text)
        {
            if (this.Handler == null) { throw new InvalidOperationException("DelegateLogger Handler must be non-null before call Log()."); }
            this.Handler(text);
        }
    }
}
