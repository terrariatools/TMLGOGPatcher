﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Logging.Loggers
{
    /// <summary>
    /// Class representing a logging object that consumes and doesn't log anything passed to it.
    /// Used when logging isn't desired, but a ILogger object is required.
    /// </summary>
    public class DummyLogger : ALogger
    {
        /// <summary>
        /// Consume an unfiltered string of text without logging it anywhere.
        /// </summary>
        /// <param name="text">String of text to consume and not log.</param>
        public override void Log(string text)
        {
        }
    }
}
