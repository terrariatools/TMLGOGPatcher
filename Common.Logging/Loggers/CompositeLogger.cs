﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

using System.Collections.Generic;

namespace Common.Logging.Loggers
{
    /// <summary>
    /// Class representing a composite of multiple logging objects.
    /// Logged records are passed to all registered loggers for processing.
    /// Records logged with a level will be filter by the CompositeLogger's
    /// filter values before being passed to the registered loggers. The
    /// CompositeLogger does not itself actually log any data to any form of
    /// output and if no loggers are registered, then logging will have no
    /// noticable effect.
    /// </summary>
    public class CompositeLogger : ALogger
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CompositeLogger()
        {
        }

        /// <summary>
        /// Get list of registered loggers.
        /// </summary>
        public List<ILogger> Loggers { get { return this._loggers; } protected set { this._loggers = value; } }
        /// <summary>
        /// Backing variable for Loggers property.
        /// </summary>
        protected List<ILogger> _loggers = new List<ILogger>();

        /// <summary>
        /// Log an unfiltered string of text to all registered loggers.
        /// </summary>
        /// <param name="text">String of text to log.</param>
        public override void Log(string text)
        {
            foreach (ILogger logger in this.Loggers) { logger.Log(text); }
        }

        /// <summary>
        /// Format and log an unfiltered string of text to all registered loggers.
        /// </summary>
        /// <param name="format">Format of log text.</param>
        /// <param name="values">Format substitution values.</param>
        public override void Log(string format, params object[] values)
        {
            foreach (ILogger logger in this.Loggers) { logger.Log(format, values); }
        }

        /// <summary>
        /// Log a record with the default format and the given level and value to all registered loggers.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="value">Text of the log record.</param>
        public override void Log(LogLevelEnums level, string value)
        {
            if (!this.IsLevelLoggable(level)) { return; }
            foreach (ILogger logger in this.Loggers) { logger.Log(level, value); }
        }

        /// <summary>
        /// Log a record with the given format, level, and values to all registered loggers.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        public override void Log(LogLevelEnums level, string format, params object[] values)
        {
            if (!this.IsLevelLoggable(level)) { return; }
            foreach (ILogger logger in this.Loggers) { logger.Log(level, format, values); }
        }
    }
}
