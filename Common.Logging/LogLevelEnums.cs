﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

using System;

namespace Common.Logging
{
    /// <summary>
    /// Enumeration representing the priority of log messages.
    /// </summary>
    [Flags]
    public enum LogLevelEnums
    {
        /// <summary>
        /// Represents no messages.
        /// </summary>
        None = 0,

        /// <summary>
        /// Lowest level. Represents temporary messages used during development.
        /// </summary>
        Debug = 1,

        /// <summary>
        /// Standard level.Represents any normal message that needs to be logged.
        /// </summary>
        Info = 2,

        /// <summary>
        /// Second highest level. Represents a failure that could be resolved.
        /// For example, a program attempts to load a settings file, but the
        /// file is not found, so a warning log message is sent and default
        /// settings are used.
        /// </summary>
        Warning = 4,

        /// <summary>
        /// Highest level. Represents a failure that could not be resolved,
        /// though that does not mean the program won't continue operations.
        /// For example, a text file could not be found so an error log message
        /// is sent and the dialog is displayed blank.
        /// </summary>
        Error = 8,

        /// <summary>
        /// Helper value combining all other values: Debug | Info | Warning | Error
        /// </summary>
        All = Debug | Info | Warning | Error
    }
}
