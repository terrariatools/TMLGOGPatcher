﻿#region File Header
// <copyright>
// Copyright (c) 2013-2014 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-12-05</date>
//
#endregion

namespace Common.Logging
{
    /// <summary>
    /// Interface representing a logging object.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Get or set the new line sequence.
        /// </summary>
        string NewLine { get; set; }

        /// <summary>
        /// Get or set format used to compose a single log record.
        /// </summary>
        string RecordFormat { get; set; }

        /// <summary>
        /// Get or set level used to filter messages.
        /// </summary>
        LogLevelEnums LevelFilter { get; set; }

        /// <summary>
        /// Check is a level will be logged.
        /// </summary>
        /// <param name="level">Level to check.</param>
        /// <returns>True if the level will be logged.</returns>
        bool IsLevelLoggable(LogLevelEnums level);

        /// <summary>
        /// Add to the level filter.
        /// </summary>
        /// <param name="level">Level to add to the filter.</param>
        void AddLevelFilter(LogLevelEnums level);

        /// <summary>
        /// Remove from the level filter.
        /// </summary>
        /// <param name="level">Level to remove from the filter.</param>
        void RemoveLevelFilter(LogLevelEnums level);

        /// <summary>
        /// Form a log record with the default format and the given level and value.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="value">Text of the log record.</param>
        /// <returns>Formated log record.</returns>
        string FormRecord(LogLevelEnums level, string value);

        /// <summary>
        /// Form a log record with the given format, level, and values.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        /// <returns>Formated log record.</returns>
        string FormRecord(LogLevelEnums level, string format, params object[] values);

        /// <summary>
        /// Log an unfiltered string of text. When overriding in derived classes,
        /// this method should accept empty string as a valid argument, append a
        /// new line to the end of the given text, and should not modify the given
        /// text in any other manner not required to transfer the text to the
        /// target of the output.
        /// All other methods should ultimately pass their content through this
        /// method to perform the actual logging operation.
        /// </summary>
        /// <param name="text">String of text to log.</param>
        void Log(string text);

        /// <summary>
        /// Log one or more blank lines of text.
        /// </summary>
        /// <param name="count">Number of blank lines to log.</param>
        void Log(int count = 1);

        /// <summary>
        /// Format and log an unfiltered string of text.
        /// </summary>
        /// <param name="format">Format of log text.</param>
        /// <param name="values">Format substitution values.</param>
        void Log(string format, params object[] values);

        /// <summary>
        /// Log a record with the default format and the given level and value.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="value">Text of the log record.</param>
        void Log(LogLevelEnums level, string value);

        /// <summary>
        /// Log a record with the given format, level, and values.
        /// </summary>
        /// <param name="level">Level of the log record.</param>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        void Log(LogLevelEnums level, string format, params object[] values);

        /// <summary>
        /// Log an error record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        void LogError(string value);

        /// <summary>
        /// Log an error record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        void LogError(string format, params object[] values);

        /// <summary>
        /// Log a warning record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        void LogWarning(string value);

        /// <summary>
        /// Log a warning record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        void LogWarning(string format, params object[] values);

        /// <summary>
        /// Log an info record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        void LogInfo(string value);

        /// <summary>
        /// Log an info record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        void LogInfo(string format, params object[] values);

        /// <summary>
        /// Log a debug record with the given value.
        /// </summary>
        /// <param name="value">Text of the log record.</param>
        void LogDebug(string value);

        /// <summary>
        /// Log a debug record with the given format and values.
        /// </summary>
        /// <param name="format">Format of log record.</param>
        /// <param name="values">Format substitution values.</param>
        void LogDebug(string format, params object[] values);
    }
}
