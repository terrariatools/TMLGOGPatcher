﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Controls.Dialogs;
using Common.Logging.Loggers;
using Newtonsoft.Json;
using TmlGogPatcher.Controls;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    public partial class MainForm : Form
    {
        public MainForm(Arguments arguments) : this()
        {
            if (arguments == null) { return; }
            foreach (string argument in arguments.Args.Keys)
            {
                AppData.Log.Log($"{argument}={(arguments.IsFlag(argument) ? arguments.GetFlag(argument).ToString() : string.Join(", ", arguments.GetList(argument)))}");
            }
        }

        public MainForm()
        {
            this.InitializeComponent();

            {
                Helper.EnsureDirectory(Paths.Patcher.Logs);
                Helper.EnsureDirectory(Paths.Patcher.Patches);

                // Begin: Convert .zip patch files to .dat patch files.
                Helper.ConvertPatchExtension(Paths.Patcher.Patches);
                // End: Convert .zip patch files to .dat patch files.
            }

            this.Icon = Icon.FromHandle(Properties.Resources.TMLGOGPatcher.GetHicon());
            this.Text = AppData.Title;

            this._textLog.Font = new Font(FontFamily.GenericMonospace, this._textLog.Font.Size);

            {
                AppData.Log.Loggers.Add(new TextBoxLogger(this._textLog));
                AppData.Log.Loggers.Add(new FileLogger(Paths.Patcher.LogFile));
            }

            {
                this.Lang = JsonConvert.DeserializeObject<LangInfo>(File.ReadAllText("./Assets/Langs/Lang.en-US.json"));
                this.Lang.Substitutions.Add("[NEWLINE]", Environment.NewLine);
                this.Lang.Substitutions.Add("[PLATFORM]", AppData.PlatformText);
                this.Lang.Substitutions.Add("[OSVERSION]", Environment.OSVersion.VersionString);
                this.Lang.Substitute();
            }

            {
                this.PatchDict.BuildDict(Paths.Patcher.Patches, AppData.PatchFileNamePattern);
            }

            {
                this._toolOpenFolderTerraria.Path = Paths.Terraria.DataRoot;
                this._toolOpenFolderTerrariaCaptures.Path = Paths.Terraria.Captures;
                this._toolOpenFolderTerrariaLogs.Path = Paths.Terraria.Logs;
                this._toolOpenFolderTerrariaPlayers.Path = Paths.Terraria.Players;
                this._toolOpenFolderTerrariaWorlds.Path = Paths.Terraria.Worlds;

                this._toolOpenFolderTml.Path = Paths.Tml.DataRoot;
                this._toolOpenFolderTmlCaptures.Path = Paths.Tml.Captures;
                this._toolOpenFolderTmlLogs.Path = Paths.Tml.Logs;
                this._toolOpenFolderTmlMods.Path = Paths.Tml.Mods;
                this._toolOpenFolderTmlSources.Path = Paths.Tml.Sources;
                this._toolOpenFolderTmlPlayers.Path = Paths.Tml.Players;
                this._toolOpenFolderTmlWorlds.Path = Paths.Tml.Worlds;

                this._toolOpenFolderPatcher.Path = Paths.Patcher.DataRoot;
                this._toolOpenFolderPatcherLogs.Path = Paths.Patcher.Logs;
                this._toolOpenFolderPatcherPatches.Path = Paths.Patcher.Patches;
            }

            {
                this._toolVisitPatcherWebsite.Url = Urls.Patcher.Website;
                this._toolVisitPatcherThread.Url = Urls.Patcher.Thread;
                this._toolVisitPatcherGitlab.Url = Urls.Patcher.Gitlab;
                this._toolVisitPatcherWiki.Url = Urls.Patcher.Wiki;

                this._toolVisitTerrariaWebsite.Url = Urls.Terraria.Website;
                this._toolVisitTerrariaForum.Url = Urls.Terraria.Forum;
                this._toolVisitTerrariaForumRules.Url = Urls.Terraria.ForumRules;
                this._toolVisitTerrariaModRules.Url = Urls.Terraria.ModRules;

                this._toolVisitTmlThread.Url = Urls.TML.Thread;
                this._toolVisitTmlGithub.Url = Urls.TML.GitHub;
                this._toolVisitTmlWiki.Url = Urls.TML.Wiki;
            }

#if WINDOWS
            this._checkOverwriteClient.Checked = true;
#elif LINUX
            this._checkOverwriteClient.Checked = true;
            this._checkOverwriteServer.Checked = true;
#elif MACOSX
            this._checkOverwriteClient.Checked = true;
            this._checkOverwriteServer.Checked = true;

            this._toolPatchDownload.Visible = false;
#else
#endif
            
            this._textPath.Text = Paths.Terraria.Install;

            this.UpdateVersionTerrariaOptions();

            AppData.PatchFolderWatcher.Created += new FileSystemEventHandler(this.PatchFolderWatcher_Changed);
            AppData.PatchFolderWatcher.Deleted += new FileSystemEventHandler(this.PatchFolderWatcher_Changed);
            AppData.PatchFolderWatcher.Changed += new FileSystemEventHandler(this.PatchFolderWatcher_Changed);
            AppData.PatchFolderWatcher.EnableRaisingEvents = true;
        }

        public LangInfo Lang { get; set; }

        public PatchInfoDictionary PatchDict { get; } = new PatchInfoDictionary();

        public TerrariaInfo TerrariaInfo { get; } = new TerrariaInfo();

        public bool Installing { get; set; }

        public string VersionTer { get { return this._comboVersionTer.Text; } }

        public string VersionTml { get { return this._comboVersionTml.Text; } }

        public bool InstallClient { get { return this._checkInstallClient.Checked; } }

        public bool InstallServer { get { return this._checkInstallServer.Checked; } }

        public bool InstallDependencies { get { return this._checkInstallDependencies.Checked; } }

        public bool OverwriteClient { get { return this._checkOverwriteClient.Checked; } }

        public bool OverwriteServer { get { return this._checkOverwriteServer.Checked; } }

        public void UpdateVersionTerrariaOptions()
        {
            this._comboVersionTer.Items.Clear();
            
            if (this.TerrariaInfo.ClientPath.Exists)
            {
                string[] keys = this.PatchDict.GetTerKeys();
                if (keys.Length > 0)
                {
                    int selected = 0;
                    for (int index = 0; index < keys.Length; index++)
                    {
                        this._comboVersionTer.Items.Add(keys[index]);
                        if (keys[index] == this.TerrariaInfo.ClientPath.Version) { selected = index; }
                    }
                    this._comboVersionTer.SelectedIndex = selected;
                }
            }

            this.UpdateVersionTmlOptions();
        }

        public void UpdateVersionTmlOptions()
        {
            this._comboVersionTml.Items.Clear();

            string[] keys = this.PatchDict.GetTmlKeys(this.VersionTer);
            if (keys != null && keys.Length > 0)
            {
                this._comboVersionTml.Items.AddRange(keys);
                this._comboVersionTml.SelectedIndex = 0;
            }

            this.UpdateInterface();
        }

        public void UpdateInterface()
        {
            bool exist = this.TerrariaInfo.DirPath.Exists && this.TerrariaInfo.ClientPath.Exists && this.TerrariaInfo.ServerPath.Exists;
            bool version = this._comboVersionTer.Items.Count > 0 && this._comboVersionTml.Items.Count > 0;

            this._textPath.Enabled = !this.Installing;
            this._buttonBrowse.Enabled = !this.Installing;
            this._buttonOpen.Enabled = !this.Installing && this.TerrariaInfo.DirPath.Exists;
            this._buttonInstall.Enabled = !this.Installing && exist && version && (this.InstallClient || this.InstallServer || this.InstallDependencies);

            this._comboVersionTer.Enabled = !this.Installing;
            this._comboVersionTml.Enabled = !this.Installing;

            this._checkInstallClient.Enabled = !this.Installing;
            this._checkInstallServer.Enabled = !this.Installing;
            this._checkInstallDependencies.Enabled = !this.Installing;

            this._checkOverwriteClient.Enabled = !this.Installing;
            this._checkOverwriteServer.Enabled = !this.Installing;

            string text = null;
            if (!this.TerrariaInfo.DirPath.Exists) { text = this.Lang["Misc", "Dir0"]; }
            else
            {
                text = string.Format(this.Lang["AppStatus", "Main"],
                    (this.TerrariaInfo.ClientPath.Exists ? this.TerrariaInfo.ClientPath.Version : this.Lang["Misc", "Found0"]),
                    (this.TerrariaInfo.ServerPath.Exists ? this.TerrariaInfo.ServerPath.Version : this.Lang["Misc", "Found0"]));
            }
            this._status.Text = text;

            this._picDirPathStatus.Image = (exist ? Properties.Resources.dir2 : (this.TerrariaInfo.DirPath.Exists ? Properties.Resources.dir1 : Properties.Resources.dir0));
            if (exist) { this._tips.SetToolTip(this._picDirPathStatus, this.Lang["PathStatus", "Valid"]); }
            else if (this.TerrariaInfo.DirPath.Exists) { this._tips.SetToolTip(this._picDirPathStatus, this.Lang["PathStatus", "Exist"]); }
            else { this._tips.SetToolTip(this._picDirPathStatus, this.Lang["PathStatus", "Invalid"]); }

            this._toolViewLog.Enabled = File.Exists(Paths.Patcher.LogFile);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void PatchFolderWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                this.PatchDict.BuildDict(Paths.Patcher.Patches, AppData.PatchFileNamePattern);
                this.UpdateVersionTerrariaOptions();
            });
            //this.Invoke((MethodInvoker)delegate() { this._textLog.AppendText("File: " + e.FullPath + " | " + e.ChangeType + Environment.NewLine); });
        }

        private void _textPath_TextChanged(object sender, EventArgs e)
        {
            this._textLog.Clear();
            this.TerrariaInfo.Set(this._textPath.Text);
            this.UpdateVersionTerrariaOptions();
        }

        private void _buttonBrowse_Click(object sender, EventArgs e) { if (AppData.DialogFolder.ShowDialog() == DialogResult.OK) { this._textPath.Text = AppData.DialogFolder.SelectedPath; } }
        
        private void _buttonOpen_Click(object sender, EventArgs e) { this.TerrariaInfo.DirPath.Open(); }

        private void _comboVersionTer_SelectedIndexChanged(object sender, EventArgs e) { this.UpdateVersionTmlOptions(); }

        private void _comboVersionTml_SelectedIndexChanged(object sender, EventArgs e) { this.UpdateInterface(); }

        private void _checkInstallClient_CheckedChanged(object sender, EventArgs e) { this.UpdateInterface(); }

        private void _checkInstallServer_CheckedChanged(object sender, EventArgs e) { this.UpdateInterface(); }

        private void _checkInstallDependencies_CheckedChanged(object sender, EventArgs e) { this.UpdateInterface(); }

        private void _doRefresh_Click(object sender, EventArgs e) { this.UpdateVersionTerrariaOptions(); }

        private void _doViewLog_Click(object sender, EventArgs e) { System.Diagnostics.Process.Start(Paths.Patcher.LogFile); }

        private void _doAbout_Click(object sender, EventArgs e) { AppData.DialogAbout.ShowDialog(this); }

        private void _buttonInstall_Click(object sender, EventArgs e)
        {
            var data = new Installer(this.Lang, AppData.Log,
                this.TerrariaInfo, this.PatchDict[this.VersionTer, this.VersionTml],
                this.InstallClient, this.InstallServer, this.InstallDependencies,
                this.OverwriteClient, this.OverwriteServer);

            using (ConfirmDialog dialog = new ConfirmDialog())
            {
                dialog.Text = data.Lang["InstallConfirmDialog", "Title"];
                dialog.ContentBackColor = Color.LightGray;
                dialog.MinimumSize = this.MinimumSize;
                dialog.ContentFont = new Font(FontFamily.GenericMonospace, this._textLog.Font.Size);
                dialog.SetIcon(SystemIconEnum.Exclamation);
                dialog.AddButtons(DialogResult.OK, DialogResult.Cancel);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Intro"]);
                dialog.AddContent();
                dialog.AddContent(data.Lang["InstallConfirmDialog", "InstallPath"], data.TerrariaInfo.DirPath);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "PatchPath"], data.PatchInfo.Path);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "TerVersion"], data.PatchInfo.VersionTerraria);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "TmlVersion"], data.PatchInfo.VersionTml);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Client"]);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Install"], data.InstallClient);
                if (data.InstallClient)
                {
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Overwrite"], data.OverwriteClient);
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Input"], data.TerrariaInfo.ClientPath.FileName);
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Output"], data.GetClientOutputText());
                }
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Server"]);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Install"], data.InstallServer);
                if (data.InstallServer)
                {
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Overwrite"], data.OverwriteServer);
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Input"], data.TerrariaInfo.ServerPath.FileName);
                    dialog.AddContent(data.Lang["InstallConfirmDialog", "Output"], data.GetServerOutputText());
                }
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Dependencies"]);
                dialog.AddContent(data.Lang["InstallConfirmDialog", "Install"], data.InstallDependencies);
                if (data.InstallDependencies)
                {
                }
                DialogResult result = dialog.ShowDialog(this);
                if (result != DialogResult.OK) { return; }
            }

            this.Installing = true;
            this.UpdateInterface();

            this._textLog.Clear();
            this._progress.Style = ProgressBarStyle.Marquee;
            this._progress.Visible = true;

            Task installer = new Task(() => data.Install(AppData.PlatformText));
            installer.ContinueWith((Task task) => { this.Invoke(new Action(this._installationTask_Completed)); });
            installer.Start();
        }

        private void _installationTask_Completed()
        {
            this._toolViewLog.Enabled = File.Exists(Paths.Patcher.LogFile);
            this._progress.Visible = false;
            this._progress.Style = ProgressBarStyle.Blocks;

            this.Installing = false;
            this.UpdateInterface();
        }

        private void _toolPatchDownload_Click(object sender, EventArgs e)
        {
            using (var dialog = new PatchDownloadDialog())
            {
                dialog.Icon = this.Icon;
                dialog.Text = this.Lang["PatchDownloadDialog", "Title"];
                dialog.ShowDialog(this);
            }
        }

        private void _toolPatchList_Click(object sender, EventArgs e)
        {
            using (var dialog = new PatchListDialog())
            {
                dialog.Icon = this.Icon;
                dialog.Text = this.Lang["PatchListDialog", "Title"];
                dialog.MinimumSize = this.MinimumSize;
                foreach (var ter in this.PatchDict.GetTerKeys())
                {
                    foreach (var tml in this.PatchDict.GetTmlKeys(ter))
                    {
                        dialog.AddPatchInfo(this.PatchDict[ter, tml]);
                    }
                }
                dialog.ShowDialog(this);
            }
        }
    }
}
