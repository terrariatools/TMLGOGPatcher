﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace TmlGogPatcher.Controls
{
    public class ToolStripUrlMenuItem : ToolStripMenuItem
    {
        public ToolStripUrlMenuItem() : base() { this.Init(); }

        public ToolStripUrlMenuItem(Image image) : base(image) { this.Init(); }

        public ToolStripUrlMenuItem(string text) : base(text) { this.Init(); }

        public ToolStripUrlMenuItem(string text, Image image) : base(text, image) { this.Init(); }

        public ToolStripUrlMenuItem(string text, Image image, EventHandler onClick) : base(text, image, onClick) { this.Init(); }

        public ToolStripUrlMenuItem(string text, Image image, params ToolStripItem[] dropDownItems) : base(text, image, dropDownItems) { this.Init(); }

        public ToolStripUrlMenuItem(string text, Image image, EventHandler onClick, Keys shortcutKeys) : base(text, image, onClick, shortcutKeys) { this.Init(); }

        public ToolStripUrlMenuItem(string text, Image image, EventHandler onClick, string name) : base(text, image, onClick, name) { this.Init(); }

        protected void Init()
        {
            this.OnUrlChanged(EventArgs.Empty);
        }

        [DefaultValue(true)]
        public bool ClickOpensUrl { get { return this._open; } set { this._open = value; } }
        private bool _open = true;

        [DefaultValue("")]
        public string Url
        {
            get { return this._url; }
            set
            {
                this._url = value;
                this.OnUrlChanged(EventArgs.Empty);
            }
        }
        private string _url = null;

        protected override void OnClick(EventArgs e)
        {
            if (this.ClickOpensUrl) { System.Diagnostics.Process.Start(this.Url); }
            base.OnClick(e);
        }

        protected virtual void OnUrlChanged(EventArgs e)
        {
            this.Enabled = !string.IsNullOrWhiteSpace(this._url);
        }
    }
}
