﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace TmlGogPatcher.Controls
{
    public class ToolStripPathMenuItem : ToolStripMenuItem
    {
        public ToolStripPathMenuItem() : base() { this.Init(); }

        public ToolStripPathMenuItem(Image image) : base(image) { this.Init(); }

        public ToolStripPathMenuItem(string text) : base(text) { this.Init(); }

        public ToolStripPathMenuItem(string text, Image image) : base(text, image) { this.Init(); }

        public ToolStripPathMenuItem(string text, Image image, EventHandler onClick) : base(text, image, onClick) { this.Init(); }

        public ToolStripPathMenuItem(string text, Image image, params ToolStripItem[] dropDownItems) : base(text, image, dropDownItems) { this.Init(); }

        public ToolStripPathMenuItem(string text, Image image, EventHandler onClick, Keys shortcutKeys) : base(text, image, onClick, shortcutKeys) { this.Init(); }

        public ToolStripPathMenuItem(string text, Image image, EventHandler onClick, string name) : base(text, image, onClick, name) { this.Init(); }

        protected void Init()
        {
            this.OnPathChanged(EventArgs.Empty);
        }

        [DefaultValue(true)]
        public bool ClickOpensPath { get { return this._open; } set { this._open = value; } }
        private bool _open = true;

        [DefaultValue("")]
        public string Path
        {
            get { return this._path; }
            set
            {
                this._path = value;
                this.OnPathChanged(EventArgs.Empty);
            }
        }
        private string _path = null;

        protected override void OnClick(EventArgs e)
        {
            if (this.ClickOpensPath) { System.Diagnostics.Process.Start(System.IO.Path.GetFullPath(this.Path)); }
            base.OnClick(e);
        }

        protected virtual void OnPathChanged(EventArgs e)
        {
            this.UpdateInterface();
        }

        public virtual void UpdateInterface()
        {
            this.Enabled = !string.IsNullOrWhiteSpace(this._path) && (System.IO.Directory.Exists(this.Path) || System.IO.File.Exists(this.Path));
        }
    }
}
