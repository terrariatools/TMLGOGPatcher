﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Common.Controls.Dialogs;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace TmlGogPatcher.Controls
{
    public partial class PatchDownloadDialog : DialogForm
    {
        public PatchDownloadDialog()
        {
            this.InitializeComponent();
            this.UpdateInterface();
        }

        public bool Downloading { get; set; }

        private WebClient Client { get; set; }

        public void UpdateInterface()
        {
            this._text.Enabled = !this.Downloading;
            this._progress.Visible = this.Downloading;
            this._buttonDownload.Enabled = !this.Downloading && !string.IsNullOrWhiteSpace(this._text.Text);// && new Uri(this._text.Text).IsFile;
            this._buttonCancel.Text = (this.Downloading ? "&Cancel" : "&Close");
        }

        private void PatchDownloadDialog_Load(object sender, EventArgs e)
        {

        }

        private void _text_TextChanged(object sender, EventArgs e)
        {
            this.UpdateInterface();
        }

        private void _buttonDownload_Click(object sender, EventArgs e)
        {
            this.Downloading = true;
            this._status.Text = "Connecting...";
            this.UpdateInterface();
            this.Refresh();

            this.Client = new WebClient();

            Uri source = new Uri(this._text.Text);
            string target = Path.Combine(Paths.Patcher.Patches, Path.GetFileName(this._text.Text));
            this.Client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this._client_DownloadProgressChanged);
            this.Client.DownloadFileCompleted += new AsyncCompletedEventHandler(this._client_DownloadFileCompleted);
            new Task(() => this.Client.DownloadFileAsync(source, target)).Start();
        }

        private void _buttonCancel_Click(object sender, EventArgs e)
        {
            if (this.Downloading) { if (this.Client != null) { this.Client.CancelAsync(); } }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }

        private void _client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                this._status.Text = string.Format("Downloading ({0}%)...", e.ProgressPercentage);
                this.Refresh();
            }));
        }

        private void _client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                this.Downloading = false;
                if (e.Cancelled) { this._status.Text = "Cancelled"; }
                else if (e.Error != null) { this._status.Text = "Error"; }
                else { this._status.Text = "Finished"; }
                this.Client.DownloadProgressChanged -= new DownloadProgressChangedEventHandler(this._client_DownloadProgressChanged);
                this.Client.DownloadFileCompleted -= new AsyncCompletedEventHandler(this._client_DownloadFileCompleted);
                this.Client.Dispose();
                this.Client = null;
                this.UpdateInterface();
                this.Refresh();
            }));
        }
    }
}
