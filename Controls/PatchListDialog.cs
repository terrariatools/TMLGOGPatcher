﻿using System;
using System.Windows.Forms;
using Common.Controls.Dialogs;

namespace TmlGogPatcher.Controls
{
    public partial class PatchListDialog : DialogForm
    {
        public PatchListDialog()
        {
            this.InitializeComponent();
        }

        private void PatchListDialog_Load(object sender, EventArgs e)
        {

        }

        public void AddPatchInfo(PatchInfo info)
        {
            var item = new PatchListItem();
            this._layout.Controls.Add(item);
            item.Width = this._layout.Width;
            item.Margin = new Padding(0, 0, 0, 4);
            item.PatchInfo = info;
        }

        private void _layout_Resize(object sender, EventArgs e)
        {
            foreach (var control in this._layout.Controls)
            {
                (control as PatchListItem).Width = this._layout.Width;
            }
        }
    }
}
