﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TmlGogPatcher.Controls
{
    public partial class PatchListItem : UserControl
    {
        public PatchListItem()
        {
            this.InitializeComponent();
        }

        public PatchInfo PatchInfo
        {
            get { return this._patchInfo; }
            set
            {
                this._patchInfo = value;
                this._labelTer.Text = $"Terraria: {(value == null ? "NONE" : value.VersionTerraria)}";
                this._labelTml.Text = $"TML: {(value == null ? "NONE" : value.VersionTml)}";
                this._labelDate.Text = $"Date: {(value == null ? "NONE" : File.GetLastWriteTime(value.Path).ToString("yyy/MM/dd HH:mm:ss"))}";
                this._labelFile.Text = $"File: {(value == null ? "NONE" : Path.GetFileName(value.Path))}";
            }
        }
        private PatchInfo _patchInfo = null;
    }
}
