﻿namespace TmlGogPatcher.Controls
{
    partial class PatchListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._labelTer = new System.Windows.Forms.Label();
            this._labelTml = new System.Windows.Forms.Label();
            this._labelDate = new System.Windows.Forms.Label();
            this._labelFile = new System.Windows.Forms.Label();
            this._table.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.AutoSize = true;
            this._table.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._table.ColumnCount = 3;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._table.Controls.Add(this._labelTer, 0, 0);
            this._table.Controls.Add(this._labelTml, 1, 0);
            this._table.Controls.Add(this._labelDate, 2, 0);
            this._table.Controls.Add(this._labelFile, 0, 1);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Margin = new System.Windows.Forms.Padding(0);
            this._table.Name = "_table";
            this._table.RowCount = 2;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._table.Size = new System.Drawing.Size(124, 41);
            this._table.TabIndex = 0;
            // 
            // _labelTer
            // 
            this._labelTer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelTer.AutoSize = true;
            this._labelTer.Location = new System.Drawing.Point(3, 3);
            this._labelTer.Margin = new System.Windows.Forms.Padding(3);
            this._labelTer.Name = "_labelTer";
            this._labelTer.Size = new System.Drawing.Size(43, 13);
            this._labelTer.TabIndex = 0;
            this._labelTer.Text = "Terraria";
            // 
            // _labelTml
            // 
            this._labelTml.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelTml.AutoSize = true;
            this._labelTml.Location = new System.Drawing.Point(52, 3);
            this._labelTml.Margin = new System.Windows.Forms.Padding(3);
            this._labelTml.Name = "_labelTml";
            this._labelTml.Size = new System.Drawing.Size(29, 13);
            this._labelTml.TabIndex = 0;
            this._labelTml.Text = "TML";
            // 
            // _labelDate
            // 
            this._labelDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelDate.AutoSize = true;
            this._labelDate.Location = new System.Drawing.Point(87, 3);
            this._labelDate.Margin = new System.Windows.Forms.Padding(3);
            this._labelDate.Name = "_labelDate";
            this._labelDate.Size = new System.Drawing.Size(30, 13);
            this._labelDate.TabIndex = 0;
            this._labelDate.Text = "Date";
            // 
            // _labelFile
            // 
            this._labelFile.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelFile.AutoSize = true;
            this._table.SetColumnSpan(this._labelFile, 3);
            this._labelFile.Location = new System.Drawing.Point(3, 23);
            this._labelFile.Margin = new System.Windows.Forms.Padding(3);
            this._labelFile.Name = "_labelFile";
            this._labelFile.Size = new System.Drawing.Size(23, 13);
            this._labelFile.TabIndex = 0;
            this._labelFile.Text = "File";
            // 
            // PatchListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._table);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MinimumSize = new System.Drawing.Size(120, 38);
            this.Name = "PatchListItem";
            this.Size = new System.Drawing.Size(124, 41);
            this._table.ResumeLayout(false);
            this._table.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _table;
        private System.Windows.Forms.Label _labelTer;
        private System.Windows.Forms.Label _labelTml;
        private System.Windows.Forms.Label _labelDate;
        private System.Windows.Forms.Label _labelFile;
    }
}
