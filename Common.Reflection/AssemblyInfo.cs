﻿#region File Header
// <copyright>
// Copyright (c) 2014-2016 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2014-04-04</date>
//
#endregion

using System;
using System.Drawing;
using System.Reflection;
using System.Globalization;

namespace Common.Reflection
{
    /// <summary>
    /// Class to wrap System.Reflection.Assembly and simplify
    /// getting common information.
    /// </summary>
    public class AssemblyInfo
    {
        /// <summary>
        /// Standard constructor.
        /// </summary>
        /// <param name="target">Assembly information to expose.</param>
        public AssemblyInfo(Assembly target)
        {
            if (target == null) { throw new ArgumentNullException("target"); }
            this.Target = target;
        }

        /// <summary>Get targeted assembly.</summary>
        public Assembly Target { get; protected set; }

        /// <summary>Get path to targeted assembly's file.</summary>
        public string Path { get { return this.Target.Location; } }

        /// <summary>Get icon associated with the entry assembly.</summary>
        public Icon Icon { get { return AssemblyHelper.GetIcon(this.Target); } }

        /// <summary>Get product text associated with the entry assembly.</summary>
        public string Product { get { return AssemblyHelper.GetProduct(this.Target); } }

        /// <summary>Get title text associated with the entry assembly.</summary>
        public string Title { get { return AssemblyHelper.GetTitle(this.Target); } }

        /// <summary>Get description text associated with the entry assembly.</summary>
        public string Description { get { return AssemblyHelper.GetDescription(this.Target); } }

        /// <summary>Get version text associated with the entry assembly.</summary>
        public string Version { get { return AssemblyHelper.GetVersion(this.Target); } }

        /// <summary>Get file version text associated with the entry assembly.</summary>
        public string FileVersion { get { return AssemblyHelper.GetFileVersion(this.Target); } }

        /// <summary>Get company text associated with the entry assembly.</summary>
        public string Company { get { return AssemblyHelper.GetCompany(this.Target); } }

        /// <summary>Get copyright text associated with the entry assembly.</summary>
        public string Copyright { get { return AssemblyHelper.GetCopyright(this.Target); } }

        /// <summary>Get trademark text associated with the entry assembly.</summary>
        public string Trademark { get { return AssemblyHelper.GetTrademark(this.Target); } }

        /// <summary>Get configuration text associated with the entry assembly.</summary>
        public string Configuration { get { return AssemblyHelper.GetConfiguration(this.Target); } }

        /// <summary>Get culture info associated with the entry assembly.</summary>
        public CultureInfo Culture { get { return AssemblyHelper.GetCulture(this.Target); } }

        /// <summary>Get guid text associated with the entry assembly.</summary>
        public string Guid { get { return AssemblyHelper.GetGuid(this.Target); } }

        /// <summary>Get comvisible value associated with the entry assembly.</summary>
        public bool ComVisible { get { return AssemblyHelper.GetComVisible(this.Target); } }
    }
}
