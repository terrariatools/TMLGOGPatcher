#region File Header
// <copyright>
// Copyright (c) 2013-2016 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-10-07</date>
//
#endregion

using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace Common.Reflection
{
    /// <summary>
    /// Helper class defining commonly used properties and methods for
    /// dealing with application information.
    /// </summary>
    public static class AppHelper
    {
        /// <summary>Reference to the entry assembly, ie the application exe.</summary>
        public static Assembly Entry { get { return Assembly.GetEntryAssembly(); } }

        /// <summary>Get icon associated with the entry assembly.</summary>
        public static Icon Icon { get { return AssemblyHelper.GetIcon(AppHelper.Entry); } }

        /// <summary>Get product text associated with the entry assembly.</summary>
        public static string Product { get { return AssemblyHelper.GetProduct(AppHelper.Entry); } }

        /// <summary>Get title text associated with the entry assembly.</summary>
        public static string Title { get { return AssemblyHelper.GetTitle(AppHelper.Entry); } }

        /// <summary>Get description text associated with the entry assembly.</summary>
        public static string Description { get { return AssemblyHelper.GetDescription(AppHelper.Entry); } }

        /// <summary>Get version text associated with the entry assembly.</summary>
        public static string Version { get { return AssemblyHelper.GetVersion(AppHelper.Entry); } }

        /// <summary>Get file version text associated with the entry assembly.</summary>
        public static string FileVersion { get { return AssemblyHelper.GetFileVersion(AppHelper.Entry); } }

        /// <summary>Get company text associated with the entry assembly.</summary>
        public static string Company { get { return AssemblyHelper.GetCompany(AppHelper.Entry); } }

        /// <summary>Get copyright text associated with the entry assembly.</summary>
        public static string Copyright { get { return AssemblyHelper.GetCopyright(AppHelper.Entry); } }

        /// <summary>Get trademark text associated with the entry assembly.</summary>
        public static string Trademark { get { return AssemblyHelper.GetTrademark(AppHelper.Entry); } }

        /// <summary>Get configuration text associated with the entry assembly.</summary>
        public static string Configuration { get { return AssemblyHelper.GetConfiguration(AppHelper.Entry); } }

        /// <summary>Get culture info associated with the entry assembly.</summary>
        public static CultureInfo Culture { get { return AssemblyHelper.GetCulture(AppHelper.Entry); } }

        /// <summary>Get guid text associated with the entry assembly.</summary>
        public static string Guid { get { return AssemblyHelper.GetGuid(AppHelper.Entry); } }

        /// <summary>Get comvisible value associated with the entry assembly.</summary>
        public static bool ComVisible { get { return AssemblyHelper.GetComVisible(AppHelper.Entry); } }

        /// <summary>Path to the current system's temporary directory.</summary>
        public static readonly string PathTemp = Path.GetTempPath().TrimEnd(new char[] { '\\' });

        /// <summary>Path to application specific directory in the system's application data directory for the current roaming user.</summary>
        public static readonly string PathAppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppHelper.Product);

        /// <summary>Path to application specific directory in the system's application data directory shared by all users.</summary>
        public static readonly string PathCommonAppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), AppHelper.Product);

        /// <summary>Object to generate random numbers.</summary>
        public static readonly Random Rand = new Random(BitConverter.ToInt32(System.Guid.NewGuid().ToByteArray(), 0));

        /// <summary>Return true if precompiler constant DEBUG is defined.</summary>
        public static bool Debug
#if DEBUG
        { get { return true; } }
#else
        { get { return false; } }
#endif

        //public static bool DesignMode { get { return false; } }
    }
}
