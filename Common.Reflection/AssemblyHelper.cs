﻿#region File Header
// <copyright>
// Copyright (c) 2016 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2016-05-12</date>
//
#endregion

using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Common.Reflection
{
    /// <summary>
    /// Helper class defining commonly used properties and methods for
    /// dealing with assembly information.
    /// </summary>
    public static class AssemblyHelper
    {
        /// <summary>
        /// Get assembly attribute of the given type T or default value for type T if the
        /// attribute is not found. In the case that more than one attribute of the given
        /// type is found, the first will be returned.
        /// </summary>
        /// <typeparam name="T">Attribute type to find.</typeparam>
        /// <param name="assembly">Assembly from which to get the custom attribute.</param>
        /// <returns>The given attribute of type T or the default value of type T.</returns>
        public static T GetCustomAttribute<T>(Assembly assembly) where T : class
        {
            object[] attributes = assembly.GetCustomAttributes(typeof(T), false);
            return (attributes != null && attributes.Length > 0 ? attributes[0] as T : default(T));
        }

        /// <summary>
        /// Get the icon associated with the assembly.
        /// </summary>
        /// <param name="assembly">Assembly from which to get the associated icon.</param>
        /// <returns>The icon associated with the assembly.</returns>
        public static Icon GetIcon(Assembly assembly) { return Icon.ExtractAssociatedIcon(assembly.Location); }

        /// <summary>
        /// Get the assembly product text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly product text or an empty string.</returns>
        public static string GetProduct(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyProductAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Product);
        }

        /// <summary>
        /// Get the assembly title text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly title text or an empty string.</returns>
        public static string GetTitle(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyTitleAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Title);
        }

        /// <summary>
        /// Get the assembly description text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly description text or an empty string.</returns>
        public static string GetDescription(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyDescriptionAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Description);
        }

        /// <summary>
        /// Get the assembly version text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly version text or an empty string.</returns>
        public static string GetVersion(Assembly assembly)
        {
            // NOTE: AssemblyVersionAttribute is not added to the assembly and
            //       tying to get it will always result in attribute being null.
            //var attribute = GetCustomAttribute<AssemblyVersionAttribute>(assembly);
            //return (attribute == null ? assembly.GetName().Version.ToString() : attribute.Version);
            return assembly.GetName().Version.ToString();
        }

        /// <summary>
        /// Get the assembly file version text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly file version text or an empty string.</returns>
        public static string GetFileVersion(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyFileVersionAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Version);
        }

        /// <summary>
        /// Get the assembly company text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly company text or an empty string.</returns>
        public static string GetCompany(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyCompanyAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Company);
        }

        /// <summary>
        /// Get the assembly copyright text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly copyright text or an empty string.</returns>
        public static string GetCopyright(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyCopyrightAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Copyright);
        }

        /// <summary>
        /// Get the assembly trademark text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly trademark text or an empty string.</returns>
        public static string GetTrademark(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyTrademarkAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Trademark);
        }

        /// <summary>
        /// Get the assembly configuration text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly configuration text or an empty string.</returns>
        public static string GetConfiguration(Assembly assembly)
        {
            var attribute = GetCustomAttribute<AssemblyConfigurationAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Configuration);
        }

        /// <summary>
        /// Get the assembly culture info.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly culture info or null.</returns>
        public static CultureInfo GetCulture(Assembly assembly)
        {
            // NOTE: AssemblyCultureAttribute is not added to the assembly and
            //       tying to get it will always result in attribute being null.
            //var attribute = GetCustomAttribute<AssemblyCultureAttribute>(assembly);
            //return (attribute == null ? string.Empty : attribute.Culture);
            return assembly.GetName().CultureInfo;
        }

        /// <summary>
        /// Get the assembly guid text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly guid text or an empty string.</returns>
        public static string GetGuid(Assembly assembly)
        {
            var attribute = GetCustomAttribute<GuidAttribute>(assembly);
            return (attribute == null ? string.Empty : attribute.Value);
        }

        /// <summary>
        /// Get the assembly culture text.
        /// </summary>
        /// <param name="assembly">Assembly from which to get information.</param>
        /// <returns>Assembly culture text or an empty string.</returns>
        public static bool GetComVisible(Assembly assembly)
        {
            var attribute = GetCustomAttribute<ComVisibleAttribute>(assembly);
            return (attribute == null ? false : attribute.Value);
        }
    }
}
