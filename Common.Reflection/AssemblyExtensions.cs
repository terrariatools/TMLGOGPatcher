#region File Header
// <copyright>
// Copyright (c) 2013-2016 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2013-10-07</date>
//
#endregion

using System.Drawing;
using System.Globalization;
using System.Reflection;

namespace Common.Reflection
{
    /// <summary>
    /// Class to provide Assembly extension methods.
    /// </summary>
    public static class AssemblyExtensions
    {
        /// <summary>
        /// Get assembly attribute of the given type T or default value for type T if the
        /// attribute is not found. In the case that more than one attribute of the given
        /// type is found, the first will be returned.
        /// </summary>
        /// <typeparam name="T">Attribute type to find.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The given attribute of type T or the default value of type T.</returns>
        public static T GetCustomAttribute<T>(this Assembly self) where T : class { return AssemblyHelper.GetCustomAttribute<T>(self); }

        /// <summary>
        /// Get the icon associated with the assembly.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The icon associated with the assembly.</returns>
        public static Icon GetIcon(this Assembly self) { return AssemblyHelper.GetIcon(self); }

        /// <summary>
        /// Get the assembly product text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly product text or an empty string.</returns>
        public static string GetProduct(this Assembly self) { return AssemblyHelper.GetProduct(self); }

        /// <summary>
        /// Get the assembly title text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly title text or an empty string.</returns>
        public static string GetTitle(this Assembly self) { return AssemblyHelper.GetTitle(self); }

        /// <summary>
        /// Get the assembly description text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly description text or an empty string.</returns>
        public static string GetDescription(this Assembly self) { return AssemblyHelper.GetDescription(self); }

        /// <summary>
        /// Get the assembly version text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly version text or an empty string.</returns>
        public static string GetVersion(this Assembly self) { return AssemblyHelper.GetVersion(self); }

        /// <summary>
        /// Get the assembly file version text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly file version text or an empty string.</returns>
        public static string GetFileVersion(this Assembly self) { return AssemblyHelper.GetFileVersion(self); }

        /// <summary>
        /// Get the assembly company text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly company text or an empty string.</returns>
        public static string GetCompany(this Assembly self) { return AssemblyHelper.GetCompany(self); }

        /// <summary>
        /// Get the assembly copyright text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly copyright text or an empty string.</returns>
        public static string GetCopyright(this Assembly self) { return AssemblyHelper.GetCopyright(self); }

        /// <summary>
        /// Get the assembly trademark text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly trademark text or an empty string.</returns>
        public static string GetTrademark(this Assembly self) { return AssemblyHelper.GetTrademark(self); }

        /// <summary>
        /// Get the assembly configuration text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly configuration text or an empty string.</returns>
        public static string GetConfiguration(this Assembly self) { return AssemblyHelper.GetConfiguration(self); }

        /// <summary>
        /// Get the assembly culture info.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly culture info or null.</returns>
        public static CultureInfo GetCulture(this Assembly self) { return AssemblyHelper.GetCulture(self); }

        /// <summary>
        /// Get the assembly guid text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly guid text or an empty string.</returns>
        public static string GetGuid(this Assembly self) { return AssemblyHelper.GetGuid(self); }

        /// <summary>
        /// Get the assembly culture text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Assembly culture text or an empty string.</returns>
        public static bool GetComVisible(this Assembly self) { return AssemblyHelper.GetComVisible(self); }
    }
}
