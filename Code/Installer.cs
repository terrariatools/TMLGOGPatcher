﻿using System;
using System.IO;
using Common.Logging;
using Ionic.Zip;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    public class Installer
    {
        public Installer(LangInfo lang, ILogger logger,
            TerrariaInfo terrariaInfo, PatchInfo patchInfo,
            bool installClient, bool installServer, bool installDependencies,
            bool overwriteClient, bool overwriteServer)
        {
            this.Lang = lang;
            this.Logger = logger;
            this.TerrariaInfo = terrariaInfo;
            this.PatchInfo = patchInfo;
            this.InstallClient = installClient;
            this.InstallServer = installServer;
            this.InstallDependencies = installDependencies;
            this.OverwriteClient = overwriteClient;
            this.OverwriteServer = overwriteServer;
        }

        /// <summary>
        /// Logger used to log messages and errors.
        /// </summary>
        public ILogger Logger { get; }

        /// <summary>
        /// LangInfo object used for logging messages.
        /// </summary>
        public LangInfo Lang { get; }

        public TerrariaInfo TerrariaInfo { get; }

        public PatchInfo PatchInfo { get; }

        public bool InstallClient { get; }

        public bool InstallServer { get; }

        public bool InstallDependencies { get; }

        public bool OverwriteClient { get; }

        public bool OverwriteServer { get; }

        /// <summary>
        /// Install client patch, server patch, and dependencies.
        /// </summary>
        public void Install(string platformText)
        {
            bool install = true;
            DateTime start, stop;

            install = this.VerifyGog();
            if (!install) { return; }

            start = DateTime.Now;

            this.Logger.Log(this.Lang["Border", "Major"]);
            this.Logger.Log(this.Lang["InstallIntro", "Started"], start);
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log(this.Lang["InstallIntro", "Platform"], platformText, Environment.OSVersion.VersionString);
            this.Logger.Log();
            this.Logger.Log(this.Lang["InstallIntro", "Patch"], this.PatchInfo.Path.FileName, this.PatchInfo.VersionTerraria, this.PatchInfo.VersionTml);
            this.Logger.Log();
            this.Logger.Log(this.Lang["InstallIntro", "Destination"], this.TerrariaInfo.DirPath);
            this.Logger.Log(this.Lang["InstallIntro", "Client"], this.InstallClient, this.OverwriteClient, this.TerrariaInfo.ClientPath.FileName, this.GetClientOutputText());
            this.Logger.Log(this.Lang["InstallIntro", "Server"], this.InstallServer, this.OverwriteServer, this.TerrariaInfo.ServerPath.FileName, this.GetServerOutputText());
            this.Logger.Log(this.Lang["InstallIntro", "Dependencies"], this.InstallDependencies);
            this.Logger.Log(this.Lang["Border", "Major"]);

            if (this.InstallClient && install) { install = this.InstallPatch(ESide.Client); }
            if (this.InstallServer && install) { install = this.InstallPatch(ESide.Server); }
            if (this.InstallDependencies && install) { install = this.InstallDependencyFiles(); }

            stop = DateTime.Now;

            this.Logger.Log();
            this.Logger.Log(this.Lang["Border", "Major"]);
            this.Logger.Log(this.Lang["InstallOutro", "Stopped"], stop);
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log(this.Lang["InstallOutro", "Elapsed"], stop - start);
            this.Logger.Log(this.Lang["Border", "Major"]);
        }

        /// <summary>
        /// Check for a Steam specific file and if that file exists then the target isn't GOG.
        /// </summary>
        public bool VerifyGog()
        {
            FilePathInfo file = new FilePathInfo(this.TerrariaInfo.DirPath, "steam_appid.txt");
            bool found = File.Exists(file);
            if (found)
            {
                this.Logger.Log(this.Lang["Border", "Major"]);
                this.Logger.Log(this.Lang["VerifyGog", "Intro"]);
                this.Logger.LogError(this.Lang["VerifyGog", "Failure"]);
                this.Logger.Log(this.Lang["Border", "Major"]);
            }
            return !found;
        }


        /// <summary>
        /// Install the client or server patch, returning true if installing should continue or false
        /// if installation should be stopped.
        /// </summary>
        /// <param name="side">Value indicating if the client or server patch is being installed.</param>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public bool InstallPatch(ESide side)
        {
            this.Logger.Log();
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log(this.Lang["InstallHeaders", (side == ESide.Client ? "Client" : "Server")], DateTime.Now);
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log();

            FilePathInfo input = this.GetInput(side);
            FilePathInfo output = this.GetOutput(side);
            FilePathInfo backup = this.GetBackup(side);
            string hashInput = this.GetHash(side, EState.Terraria);
            string hashOutput = this.GetHash(side, EState.Tml);

            EStepResult result;

            result = this.FindRequiredFile(input);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            result = this.CheckOutput(output, hashOutput);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            result = this.CheckVersion(input, this.PatchInfo.VersionTerraria);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            result = this.CheckSourceHash(input, hashInput);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            result = this.MakeBackup(input, backup, hashInput);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            result = this.ApplyPatch(input, output, this.GetDiff(side), hashOutput);
            if (result != EStepResult.Continue) { return (result == EStepResult.HaltPhase); }

            return true;
        }

        /// <summary>
        /// Install the dependency files, returning true if installing should continue or false
        /// if installation should be stopped.
        /// </summary>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public bool InstallDependencyFiles()
        {
            this.Logger.Log();
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log(this.Lang["InstallHeaders", "Dependencies"], DateTime.Now);
            this.Logger.Log(this.Lang["Border", "Minor"]);
            this.Logger.Log();

            using (ZipFile zip = this.PatchInfo.GetZip())
            {
                string source = this.PatchInfo.Root + "/Files/";
                foreach (ZipEntry e in zip)
                {
                    if (e.FileName == source || !e.FileName.StartsWith(source)) { continue; }
                    string target = Path.GetFullPath(Path.Combine(this.TerrariaInfo.DirPath, e.FileName.Replace(source, "")));
                    if (e.IsDirectory)
                    {
                        bool result = this.EnsureDirectory(target);
                        if (!result) { return false; }
                    }
                    else
                    {
                        byte[] bytes = this.PatchInfo.GetBytes(e.FileName);
                        bool result = this.EnsureDependency(target, bytes);
                        if (!result) { return false; }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Ensure that given directory exists and create it if it does not exist.
        /// If the directory doesn't exist and can not be created, then
        /// the installation process will be stopped.
        /// </summary>
        /// <param name="path">Path of the directory to ensure exists.</param>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public bool EnsureDirectory(string path)
        {
            this.Logger.Log(this.Lang["EnsureDirectory", "Intro"], path);
            bool found = Directory.Exists(path);
            this.Logger.Log("-" + this.Lang["Misc", (found ? "Found1" : "Found0")]);
            if (found) { return true; }

            try { Directory.CreateDirectory(path); }
            catch (Exception exp)
            {
                this.Logger.Log("--" + this.Lang["EnsureDirectory", "Failure"]);
                this.Logger.LogError("---" + exp.Message);
                return false;
            }
            this.Logger.Log("-" + this.Lang["EnsureDirectory", "Success"]);
            return true;
        }

        /// <summary>
        /// Ensure that the given dependency file exists at the given path and create it if
        /// it does not exist or does not match the given bytes.
        /// If the file doesn't exist or doesn't match the given bytes and can not be
        /// created, then the installation process will be stopped.
        /// </summary>
        /// <param name="path">Path of the file to ensure exists.</param>
        /// <param name="bytes">Byte array of the file that should exist.</param>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public bool EnsureDependency(string path, byte[] bytes)
        {
            string file = Path.GetFileName(path);
            this.Logger.Log(this.Lang["EnsureDependency", "Intro"], file);
            bool found = File.Exists(path);
            this.Logger.Log("-" + this.Lang["Misc", (found ? "Found1" : "Found0")]);
            if (found)
            {
                this.Logger.Log("-" + this.Lang["EnsureDependency", "Hash"], file);
                bool match = string.Equals(Helper.GetHashText(path, true), Helper.GetHashText(bytes, true));
                this.Logger.Log("--" + this.Lang["Misc", (match ? "Match1" : "Match0")]);
                if (match)
                {
                    this.Logger.Log("--" + this.Lang["EnsureDependency", "Skip"]);
                    return true;
                }
            }

            try { File.WriteAllBytes(path, bytes); }
            catch (Exception exp)
            {
                this.Logger.Log("--" + this.Lang["EnsureDependency", "Failure"]);
                this.Logger.LogError("---" + exp.Message);
                return false;
            }
            this.Logger.Log("-" + this.Lang["EnsureDependency", "Success"]);
            return true;
        }

        /// <summary>
        /// Find the given file and stop installation if it isn't found.
        /// </summary>
        /// <param name="input">Path info for the file to find.</param>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public EStepResult FindRequiredFile(FilePathInfo input)
        {
            this.Logger.Log(this.Lang["FindRequiredFile", "Intro"], input.FileName);
            bool result = input.Exists;
            this.Logger.Log("-" + this.Lang["Misc", (result ? "Found1" : "Found0")]);
            if (!result)
            {
                this.Logger.Log("-" + this.Lang["FindRequiredFile", "Failure"]);
                return EStepResult.HaltAll;
            }
            return EStepResult.Continue;
        }

        /// <summary>
        /// Check if the given output file exists and has already been patched.
        /// If the output exists and is patched, then this phase of installation
        /// will be skipped, otherwise the phase will continue normally.
        /// </summary>
        /// <param name="output">Path info for the output file that will be generated from patching.</param>
        /// <param name="hashOutput">Hash that the patched output should match.</param>
        public EStepResult CheckOutput(FilePathInfo output, string hashOutput)
        {
            this.Logger.Log(this.Lang["CheckOutput", "Intro"], output.FileName, hashOutput);
            if (output.Exists && string.Equals(output.Hash, hashOutput))
            {
                this.Logger.Log("-" + this.Lang["CheckOutput", "Skip"], output.FileName);
                return EStepResult.HaltPhase;
            }
            this.Logger.Log("-" + this.Lang["CheckOutput", "Continue"]);
            return EStepResult.Continue;
        }

        /// <summary>
        /// Check that the given file matches the given version.
        /// Installation will NOT be stopped if the version does not match, because
        /// the version of Terraria is not always updated correctly, especially with hotfixes.
        /// </summary>
        /// <param name="input">Path info for the file to find.</param>
        /// <param name="version">Version to match against the file.</param>
        public EStepResult CheckVersion(FilePathInfo input, string version)
        {
            this.Logger.Log(this.Lang["CheckVersion", "Intro"], input.FileName, version);
            bool result = string.Equals(version, input.Version);
            this.Logger.Log("-" + (result ? this.Lang["Misc", "Match1"] : string.Format(this.Lang["Misc", "Match2"], input.Version)));
            /*
            // NOTE: ReLogic sometimes forgets to increment the version when updates are release,
            //       so it may still be the correct binary even if the version isn't correct.
            if (!result)
            {
                this.Logger.Log("-" + this.Lang.CheckVersion["Failure"]);
                return EStepResult.HaltAll;
            }
            */
            return EStepResult.Continue;
        }

        /// <summary>
        /// Check that the given input file matches the given hash. If the file matches the input hash,
        /// then continue, otherwise stop the installation.
        /// </summary>
        /// <param name="input">Path info for the file to find.</param>
        /// <param name="hashInput">Hash of an unpatched file.</param>
        /// <returns>True if the installation should continue or false if installation should be stopped.</returns>
        public EStepResult CheckSourceHash(FilePathInfo input, string hashInput)
        {
            this.Logger.Log(this.Lang["CheckSourceHash", "Intro"], input.FileName, hashInput);
            bool result = string.Equals(hashInput, input.Hash);
            this.Logger.Log("-" + (result ? this.Lang["Misc", "Match1"] : string.Format(this.Lang["Misc", "Match2"], input.Hash)));
            if (!result)
            {
                this.Logger.Log("-" + this.Lang["CheckSourceHash", "Failure"]);
                return EStepResult.HaltAll;
            }
            return EStepResult.Continue;
        }

        /// <summary>
        /// Make a backup copy of the given file.
        /// If the backup file already exists and matches the given hash, then creation of
        /// the backup will be skipped. If there is a problem creating the backup, then
        /// installation will be stopped.
        /// </summary>
        /// <param name="input">Path info for the file to find.</param>
        /// <param name="backup">Path info for the file to use as the backup.</param>
        /// <param name="hash">Hash of an unpatched file that the backup file should match.</param>
        public EStepResult MakeBackup(FilePathInfo input, FilePathInfo backup, string hash)
        {
            this.Logger.Log(this.Lang["MakeBackup", "Intro"], input.FileName, backup.FileName);
            bool found = backup.Exists;
            this.Logger.Log("-" + this.Lang["Misc", (found ? "Found1" : "Found0")]);
            if (found)
            {
                this.Logger.Log("--" + this.Lang["MakeBackup", "Hash"], input.FileName, backup.FileName, hash);
                bool match = string.Equals(hash, backup.Hash);
                this.Logger.Log("---" + this.Lang["Misc", (match ? "Match1" : "Match0")]);
                if (match)
                {
                    this.Logger.Log("---" + this.Lang["MakeBackup", "Skip"]);
                    return EStepResult.Continue;
                }
            }

            this.Logger.Log("--" + this.Lang["MakeBackup", "Create"], input.FileName, backup.FileName);
            try { File.Copy(input, backup, true); }
            catch (Exception exp)
            {
                this.Logger.Log("---" + this.Lang["MakeBackup", "Failure"]);
                this.Logger.LogError("----" + exp.Message);
                return EStepResult.HaltAll;
            }
            this.Logger.Log("---" + this.Lang["MakeBackup", "Success"]);
            return EStepResult.Continue;
        }

        /// <summary>
        /// Patch the given input file with the given patch creating the given output file and
        /// verify that the generated output file matches the given hash. If the file can not
        /// be patched or the output file does not match the hash, then installation will be stopped.
        /// </summary>
        /// <param name="input">Path info used for display purposes.</param>
        /// <param name="output">Path info for the output file that will be generated from patching.</param>
        /// <param name="patch">Patch used to generate output from input.</param>
        /// <param name="hashOutput">Hash that the patched output should match.</param>
        public EStepResult ApplyPatch(FilePathInfo input, FilePathInfo output, byte[] patch, string hashOutput)
        {
            this.Logger.Log(this.Lang["ApplyPatch", "Intro"], input.FileName, output.FileName);
            using (MemoryStream streamInput = new MemoryStream(File.ReadAllBytes(input)))
            {
                using (FileStream streamOutput = new FileStream(output, FileMode.Create))
                {
                    try { BinaryPatchUtility.Apply(streamInput, () => new MemoryStream(patch), streamOutput); }
                    catch (Exception exp)
                    {
                        this.Logger.Log("-" + this.Lang["ApplyPatch", "ApplyFailure"]);
                        this.Logger.LogError("--" + exp.Message);
                        return EStepResult.HaltAll;
                    }
                    this.Logger.Log("-" + this.Lang["ApplyPatch", "ApplySuccess"]);
                }
            }

            this.Logger.Log("--" + this.Lang["ApplyPatch", "Verify"], input.FileName, output.FileName);
            bool result = string.Equals(hashOutput, output.Hash);
            if (!result)
            {
                this.Logger.Log("---" + this.Lang["ApplyPatch", "VerifyFailure"]);
                return EStepResult.HaltAll;
            }
            this.Logger.Log("---" + this.Lang["ApplyPatch", "VerifySuccess"]);
            return EStepResult.Continue;
        }

        public bool GetOverwrite(ESide side) { return (side == ESide.Client ? this.OverwriteClient : this.OverwriteServer); }

        public FilePathInfo GetInput(ESide side) { return (side == ESide.Client ? this.TerrariaInfo.ClientPath : this.TerrariaInfo.ServerPath); }

        public FilePathInfo GetOutput(ESide side) { return (this.GetOverwrite(side) ? this.GetInput(side) : (side == ESide.Client ? this.TerrariaInfo.TmlClientPath : this.TerrariaInfo.TmlServerPath)); }

        public FilePathInfo GetBackup(ESide side) { return (side == ESide.Client ? this.TerrariaInfo.ClientBackupPath : this.TerrariaInfo.ServerBackupPath); }

        public string GetVersion(EState state) { return (state == EState.Terraria ? this.PatchInfo.VersionTerraria : this.PatchInfo.VersionTml); }

        public string GetHash(ESide side, EState state) { if (side == ESide.Client) { return (state == EState.Terraria ? this.PatchInfo.HashClientTerraria : this.PatchInfo.HashClientTml); } else { return (state == EState.Terraria ? this.PatchInfo.HashServerTerraria : this.PatchInfo.HashServerTml); } }

        public byte[] GetDiff(ESide side) { return (side == ESide.Client ? this.PatchInfo.GetClientDiff() : this.PatchInfo.GetServerDiff()); }

        public string GetClientOutputText()
        {
            if (!this.InstallClient) { return this.Lang["Misc", "None"]; }
            else if (this.OverwriteClient) { return this.TerrariaInfo.ClientPath.FileName; }
            else { return this.TerrariaInfo.TmlClientPath.FileName; }
        }

        public string GetServerOutputText()
        {
            if (!this.InstallServer) { return this.Lang["Misc", "None"]; }
            else if (this.OverwriteServer) { return this.TerrariaInfo.ServerPath.FileName; }
            else { return this.TerrariaInfo.TmlServerPath.FileName; }
        }
    }
}
