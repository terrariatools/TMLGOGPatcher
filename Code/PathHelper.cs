﻿using System;
using System.IO;

namespace TmlGogPatcher
{
    public static class Paths
    {
        public static class Terraria
        {
#if LINUX
            public static string Install { get; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "GOG Games", "Terraria", "game");
            public static string DataRoot { get; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), ".local", "share", "Terraria");
#elif MACOSX
            public static string Install { get; } = Path.Combine("/Applications", "Terraria.app", "Contents", "MacOS");
            public static string DataRoot { get; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Library", "Application Support", "Terraria");
#else
#if DEBUG
            public static string Install { get; } = "D:/GOG Games/Test1344_0923/";
#else
            public static string Install { get; } = "C:/GOG Games/Terraria/";
#endif
            public static string DataRoot { get; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "My Games", "Terraria");
#endif

            public static string Captures { get; } = Path.Combine(Paths.Terraria.DataRoot, "Captures");

            public static string Logs { get; } = Path.Combine(Paths.Terraria.DataRoot, "Logs");

            public static string Players { get; } = Path.Combine(Paths.Terraria.DataRoot, "Players");

            public static string Worlds { get; } = Path.Combine(Paths.Terraria.DataRoot, "Worlds");
        }

        public static class Tml
        {
            public static string DataRoot { get; } = Path.Combine(Paths.Terraria.DataRoot, "ModLoader");

            public static string Captures { get; } = Path.Combine(Paths.Tml.DataRoot, "Captures");

            public static string Logs { get; } = Path.Combine(Paths.Tml.DataRoot, "Logs");

            public static string Mods { get; } = Path.Combine(Paths.Tml.DataRoot, "Mods");

            public static string Sources { get; } = Path.Combine(Paths.Tml.DataRoot, "Mod Sources");

            public static string Players { get; } = Path.Combine(Paths.Tml.DataRoot, "Players");

            public static string Worlds { get; } = Path.Combine(Paths.Tml.DataRoot, "Worlds");
        }

        public static class Patcher
        {
            public static string DataRoot { get; } = Path.Combine(Paths.Terraria.DataRoot, "TMLGOGPatcher");

            public static string Logs { get; } = Path.Combine(Paths.Patcher.DataRoot, "Logs");

            public static string Patches { get; } = Path.Combine(Paths.Patcher.DataRoot, "Patches");

            public static string LogFile { get; } = Path.Combine(Paths.Patcher.Logs, "Log.txt");

            public static string PatchListFile { get; } = Path.Combine(Paths.Patcher.Patches, "PatchList_" + AppData.PlatformText + ".json");
        }
    }
}
