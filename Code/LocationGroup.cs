﻿using System;
using System.Collections.Generic;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    public class LocationGroup
    {
        public LocationGroup() { }

        public LocationGroup(string specialFolder, string rootPath)
        {
            this.SpecialFolder = specialFolder;
            this.RootPath = rootPath;
        }

        public string SpecialFolder { get; set; }

        public string RootPath { get; set; }

        public Dictionary<string, string> Paths { get; set; }

        public string this[string title] { get { return (!this.Has(title) ? null : this.GetPath(title)); } }

        public void AddPath(string title, params string[] parts)
        {
            if (this.Paths == null) { this.Paths = new Dictionary<string, string>(); }
            this.Paths.Add(title, Helper.Combine(parts));
        }

        public bool Has(string title) { return this.Paths.ContainsKey(title); }

        public string GetSpecialFolderPath()
        {
            if (string.IsNullOrWhiteSpace(this.SpecialFolder)) { return null; }
            var e = (Environment.SpecialFolder)Enum.Parse(typeof(Environment.SpecialFolder), this.SpecialFolder, true);
            return Environment.GetFolderPath(e);
        }

        public string GetPath()
        {
            if (this.GetSpecialFolderPath() != null && !string.IsNullOrWhiteSpace(this.RootPath))
            {
                return Helper.Combine(this.GetSpecialFolderPath(), this.RootPath);
            }
            else if (this.GetSpecialFolderPath() != null) { return this.GetSpecialFolderPath(); }
            else if (!string.IsNullOrWhiteSpace(this.RootPath)) { return this.RootPath; }
            else { return null; }
        }

        public string GetPath(string title)
        {
            if (this.Paths == null || !this.Paths.ContainsKey(title)) { return null; }
            if (this.GetPath() == null) { return this.Paths[title]; }
            else { return Helper.Combine(this.GetPath(), this.Paths[title]); }
        }
    }
}
