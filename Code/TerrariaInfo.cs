﻿using System.IO;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    public class TerrariaInfo
    {
        public TerrariaInfo() { this.Clear(); }

        public DirPathInfo DirPath { get; } = new DirPathInfo();

        public FilePathInfo ClientPath { get; } = new FilePathInfo();

        public FilePathInfo ClientBackupPath { get; } = new FilePathInfo();

        public FilePathInfo ServerPath { get; } = new FilePathInfo();

        public FilePathInfo ServerBackupPath { get; } = new FilePathInfo();

        public FilePathInfo TmlClientPath { get; } = new FilePathInfo();

        public FilePathInfo TmlServerPath { get; } = new FilePathInfo();

        public void Clear()
        {
            this.DirPath.Target = null;
            this.ClientPath.Target = null;
            this.ClientBackupPath.Target = null;
            this.ServerPath.Target = null;
            this.ServerBackupPath.Target = null;
            this.TmlClientPath.Target = null;
            this.TmlServerPath.Target = null;
        }

        public void Set(string dir)
        {
            this.Clear();

            this.DirPath.Target = dir;
            this.ClientPath.Target = Path.Combine(this.DirPath, "Terraria.exe");
            this.ClientBackupPath.Target = Path.Combine(this.DirPath, $"Terraria_{this.ClientPath.Version}.exe");
            this.ServerPath.Target = Path.Combine(this.DirPath, "TerrariaServer.exe");
            this.ServerBackupPath.Target = Path.Combine(this.DirPath, $"TerrariaServer_{this.ServerPath.Version}.exe");
            this.TmlClientPath.Target = Path.Combine(this.DirPath, "tModLoader.exe");
            this.TmlServerPath.Target = Path.Combine(this.DirPath, "tModLoaderServer.exe");
        }
    }
}
