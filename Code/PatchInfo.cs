﻿using System;
using Ionic.Zip;
using Newtonsoft.Json;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    public class PatchInfo
    {
        public static PatchInfo FromZip(string path)
        {
            string root = System.IO.Path.GetFileNameWithoutExtension(path);
            //bool locked;
            //do { locked = Helper.IsFileLocked(path); }
            //while (locked);
            try
            {
                using (ZipFile zip = ZipFile.Read(path))
                {
                    using (Ionic.Crc.CrcCalculatorStream stream = zip[root + "/Info.json"].OpenReader())
                    {
                        using (var reader = new System.IO.StreamReader(stream))
                        {
                            PatchInfo info = JsonConvert.DeserializeObject<PatchInfo>(reader.ReadToEnd());
                            info.Path.Target = path;
                            info.Root = root;
                            return info;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected PatchInfo() { }

        [JsonIgnore]
        public FilePathInfo Path { get; } = new FilePathInfo();

        [JsonIgnore]
        public string Root { get; set; }

        public string VersionTerraria { get; set; }

        public string VersionTml { get; set; }

        public string HashClientTerraria { get; set; }

        public string HashClientTml { get; set; }

        public string HashServerTerraria { get; set; }

        public string HashServerTml { get; set; }

        public ZipFile GetZip() { return (!this.Path.Exists ? null : ZipFile.Read(this.Path)); }

        public byte[] GetBytes(string file)
        {
            using (ZipFile zip = this.GetZip())
            {
                if (zip == null) { return null; }
                using (Ionic.Crc.CrcCalculatorStream stream = zip[file].OpenReader())
                {
                    using (var reader = new System.IO.BinaryReader(stream))
                    {
                        using (var memory = new System.IO.MemoryStream())
                        {
                            int count;
                            byte[] buffer = new byte[4096];
                            while ((count = reader.Read(buffer, 0, buffer.Length)) != 0) { memory.Write(buffer, 0, count); }
                            return memory.ToArray();
                        }
                    }
                }
            }
        }

        public byte[] GetClientDiff() { return (!this.Path.Exists ? null : this.GetBytes(this.Path.FileNameWithoutExtension + "/" + "ClientDiff.bsdiff")); }

        public byte[] GetServerDiff() { return (!this.Path.Exists ? null : this.GetBytes(this.Path.FileNameWithoutExtension + "/" + "ServerDiff.bsdiff")); }
    }
}
