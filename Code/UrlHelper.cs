﻿namespace TmlGogPatcher
{
    public static class Urls
    {
        public static class Patcher
        {
            public static readonly string Website = "http://terraria.vinlandsolutions.net/tools/tmlgogpatcher/";

            public static readonly string Thread = "http://forums.terraria.org/index.php?threads/tml-gog-patcher.48470/";

            public static readonly string Gitlab = "https://gitlab.com/terrariatools/TMLGOGPatcher";

            public static readonly string Wiki = "http://wiki.vinlandsolutions.net/doku.php/terraria/tools/tml_gog_patcher";

            public static readonly string Patches = Urls.Patcher.Website + "patches/";

            public static readonly string PatchList = Urls.Patcher.Patches + "PatchList_" + AppData.PlatformText + ".json";
        }

        public static class Terraria
        {
            public static readonly string Website = "http://terraria.org";

            public static readonly string Forum = "http://forums.terraria.org/index.php?forums/";

            public static readonly string ForumRules = "http://forums.terraria.org/index.php?forums/rules.87/";

            public static readonly string ModRules = "http://forums.terraria.org/index.php?threads/player-created-game-enhancements-rules-guidelines.286/";
        }

        public static class TML
        {
            public static readonly string Thread = "http://forums.terraria.org/index.php?threads/tmodloader.23726/";

            public static readonly string GitHub = "https://github.com/bluemagic123/tModLoader";

            public static readonly string Wiki = "https://github.com/bluemagic123/tModLoader/wiki";
        }
    }
}
