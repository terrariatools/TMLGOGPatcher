﻿using System;
using System.IO;
using Common.Controls.Dialogs;
using Common.Logging.Loggers;
using Common.Reflection;
using Ookii.Dialogs;

namespace TmlGogPatcher
{
    public static class AppData
    {
        static AppData()
        {
            {
                AppData.DialogAbout.SetIcon(Properties.Resources.About);
                AppData.DialogAbout.Logo = Properties.Resources.TMLGOGPatcher;
                AppData.DialogAbout.SetUrl("http://forums.terraria.org/index.php?threads/tml-gog-patcher.50413/", "TML GOG Patcher");
                AppData.DialogAbout.SetEmail("jeckelmail@gmail.com", "jeckelmail@gmail.com");

                AppData.DialogAbout.AddTextPage("Licenses", Properties.Resources.Licenses, Properties.Resources.License);
            }

            {
                AppData.PatchFolderWatcher.Path = Paths.Patcher.Patches;
                AppData.PatchFolderWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;
                AppData.PatchFolderWatcher.Filter = AppData.PatchFileNamePattern;
            }
        }

#if LINUX
        public static readonly PlatformID Platform = PlatformID.Unix;
        public static readonly string PlatformText = "Linux";
#elif MACOSX
        public static readonly PlatformID Platform = PlatformID.MacOSX;
        public static readonly string PlatformText = "Mac";
#else
        public static readonly PlatformID Platform = PlatformID.Win32Windows;
        public static readonly string PlatformText = "Windows";
#endif

#if DEBUG
        public static readonly string Title = string.Format("TML GOG Patcher v{0} {1} DEBUG", AppHelper.Version, AppData.PlatformText);
#else
        public static readonly string Title = string.Format("TML GOG Patcher v{0} {1}", AppHelper.Version, AppData.PlatformText);
#endif

        public static string PatchFileNamePattern { get; } = string.Format("TMLGOGPatch_*_{0}.dat", AppData.PlatformText);

        public static CompositeLogger Log { get; } = new CompositeLogger();

        public static FileSystemWatcher PatchFolderWatcher { get; } = new FileSystemWatcher();

        public static VistaFolderBrowserDialog DialogFolder { get; } = new VistaFolderBrowserDialog();

        public static AboutDialog DialogAbout { get; } = new AboutDialog(AppHelper.Entry);
    }
}
