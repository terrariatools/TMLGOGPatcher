﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TmlGogPatcher
{
    public class PatchInfoDictionary
    {
        public PatchInfoDictionary()
        {
            this.Dict = new Dictionary<string, Dictionary<string, PatchInfo>>();
        }

        protected Dictionary<string, Dictionary<string, PatchInfo>> Dict { get; set; }

        public PatchInfo this[string terVersion, string tmlVersion] { get { return (this.Dict.ContainsKey(terVersion) && this.Dict[terVersion].ContainsKey(tmlVersion) ? this.Dict[terVersion][tmlVersion] : null); } }

        public bool ContainsTerKey(string terVersion) { return this.Dict.ContainsKey(terVersion); }

        public string[] GetTerKeys()
        {
            string[] keys = this.Dict.Keys.ToArray();
            if (keys.Length > 0)
            {
                Array.Sort(keys);
                Array.Reverse(keys);
            }
            return keys;
        }

        public bool ContainsTmlKey(string terVersion, string tmlVersion) { return this.Dict.ContainsKey(terVersion) && this.Dict[terVersion].ContainsKey(tmlVersion); }

        public string[] GetTmlKeys(string terVersion)
        {
            if (!this.Dict.ContainsKey(terVersion)) { return null; }
            string[] keys = this.Dict[terVersion].Keys.ToArray();
            if (keys.Length > 0)
            {
                Array.Sort(keys);
                Array.Reverse(keys);
            }
            return keys;
        }

        public void BuildDict(string dir, string pattern)
        {
            this.Dict.Clear();

            string[] patches = Directory.GetFiles(dir, pattern);
            if (patches.Length > 0)
            {
                for (int index = 0; index < patches.Length; index++)
                {
                    PatchInfo info = PatchInfo.FromZip(patches[index]);
                    if (info == null) { continue; }
                    if (!this.Dict.ContainsKey(info.VersionTerraria)) { this.Dict[info.VersionTerraria] = new Dictionary<string, PatchInfo>(); }
                    this.Dict[info.VersionTerraria][info.VersionTml] = info;
                }
            }
        }
    }
}
