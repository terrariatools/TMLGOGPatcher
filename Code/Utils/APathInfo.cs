﻿using System.IO;

namespace TmlGogPatcher.Utils
{
    public abstract class APathInfo
    {
        public APathInfo() { }

        public APathInfo(string path) : this() { this.Target = path; }

        public string Target
        {
            get { return this._target; }
            set
            {
                this._target = (string.IsNullOrWhiteSpace(value) ? string.Empty : Path.GetFullPath(value));
            }
        }
        private string _target = null;

        public abstract bool Exists { get; }

        public static implicit operator string(APathInfo info) { return info.Target; }

        public static bool operator ==(APathInfo left, APathInfo right)
        {
            if (object.ReferenceEquals(left, right)) { return true; }
            if ((object)left == null || (object)right == null) { return false; }
            return left.Target == right.Target;
        }

        public static bool operator !=(APathInfo left, APathInfo right) { return !(left == right); }

        public override string ToString() { return this.Target; }

        public override int GetHashCode() { return this.Target.GetHashCode(); }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            var path = obj as APathInfo;
            if ((object)path == null) { return false; }
            return this.Target == path.Target;
        }

        public bool Equals(APathInfo path)
        {
            if ((object)path == null) { return false; }
            return this.Target == path.Target;
        }
    }
}
