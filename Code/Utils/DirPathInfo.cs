﻿using System.IO;

namespace TmlGogPatcher.Utils
{
    public class DirPathInfo : APathInfo
    {
        public DirPathInfo() : base() { }

        public DirPathInfo(string path) : base(path) { }

        public DirPathInfo(params string[] parts) : base(Path.Combine(parts)) { }

        public override bool Exists { get { return !string.IsNullOrWhiteSpace(this.Target) && Directory.Exists(this.Target); } }

        public void Open() { if (this.Exists) { System.Diagnostics.Process.Start(this); } }
    }
}
