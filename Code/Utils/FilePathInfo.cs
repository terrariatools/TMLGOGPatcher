﻿using System.IO;

namespace TmlGogPatcher.Utils
{
    public class FilePathInfo : APathInfo
    {
        public FilePathInfo() : base() { }

        public FilePathInfo(string path) : base(path) { }

        public FilePathInfo(params string[] parts) : base(Path.Combine(parts)) { }

        public override bool Exists { get { return !string.IsNullOrWhiteSpace(this.Target) && File.Exists(this.Target); } }

        public string Version { get { return (this.Exists ? Helper.GetVersion(this.Target) : null); } }

        public string Hash { get { return (this.Exists ? Helper.GetHashText(this.Target) : null); } }

        public string Directory { get { return Path.GetDirectoryName(this); } }

        public string FileName { get { return Path.GetFileName(this); } }

        public string FileNameWithoutExtension { get { return Path.GetFileNameWithoutExtension(this); } }

        public string Extension { get { return Path.GetExtension(this).TrimStart('.'); } }
    }
}
