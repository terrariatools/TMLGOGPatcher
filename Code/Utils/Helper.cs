﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;

namespace TmlGogPatcher.Utils
{
    public enum ESide : int { Client, Server }

    public enum EState : int { Terraria, Tml }

    public enum EStepResult : int { Continue, HaltPhase, HaltAll }

    public static class Helper
    {
        public static HashAlgorithm Hasher { get; } = SHA256.Create();//SHA1.Create();

        public static byte[] GetHash(byte[] bytes) { return Helper.Hasher.ComputeHash(bytes); }

        public static byte[] GetHash(string path) { return Helper.Hasher.ComputeHash(File.ReadAllBytes(path)); }

        public static string GetHashText(byte[] bytes, bool stripDashes = true)
        {
            string hash = BitConverter.ToString(Helper.Hasher.ComputeHash(bytes));
            return (stripDashes ? hash.Replace("-", string.Empty) : hash);
        }

        public static string GetHashText(string path, bool stripDashes = true)
        {
            string hash = BitConverter.ToString(Helper.Hasher.ComputeHash(File.ReadAllBytes(path)));
            return (stripDashes ? hash.Replace("-", string.Empty) : hash);
        }

        public static string GetVersion(string path)
        {
            FileVersionInfo info = FileVersionInfo.GetVersionInfo(path);
            return string.Format("{0}.{1}.{2}.{3}", info.FileMajorPart, info.FileMinorPart, info.FileBuildPart, info.FilePrivatePart);
        }

        public static void EnsureDirectory(string path) { if (!Directory.Exists(path)) { Directory.CreateDirectory(path); } }

        public static bool IsFileLocked(string path)
        {
            FileStream stream = null;
            try { stream = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None); }
            catch (IOException) { return true; }
            finally { if (stream != null) { stream.Close(); } }
            return false;
        }

        public static DateTime GetLastModified(Uri uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK) { return response.LastModified.ToUniversalTime(); }
            return DateTime.MinValue.ToUniversalTime();
        }

        public static DateTime GetLastModified(string path)
        {
            if (File.Exists(path)) { return File.GetLastWriteTimeUtc(path); }
            return DateTime.MinValue.ToUniversalTime();
        }

        public static string Combine(params string[] parts) { return string.Join("/", parts); }

        public static string Combine(IEnumerable parts) { return string.Join("/", parts.Cast<object>().ToArray()); }

        public static void ConvertPatchExtension(string dir)
        {
            string[] files = System.IO.Directory.GetFiles(dir, "TMLGOGPatch_*_*.zip", System.IO.SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                string root = System.IO.Path.GetDirectoryName(file);
                string name = System.IO.Path.GetFileNameWithoutExtension(file);
                File.Move(file, System.IO.Path.Combine(root, name + ".dat"));
            }
        }
    }
}
