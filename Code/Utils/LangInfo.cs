﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace TmlGogPatcher.Utils
{
    public class LangInfo
    {
        [JsonIgnore]
        public Dictionary<string, string> Substitutions { get; } = new Dictionary<string, string>();

        public string LangCode { get; set; }

        public Dictionary<string, Dictionary<string, string>> Localisations { get; set; }

        public Dictionary<string, string> this[string category] { get { return (this.Has(category) ? this.Localisations[category] : null); } }

        public string this[string category, string key]
        {
            get { return (this.Has(category, key) ? this.Localisations[category][key] : null); }
            set { this.Add(category, key, value); }
        }

        public bool Has(string category) { return this.Localisations.ContainsKey(category); }

        public bool Has(string category, string key) { return this.Has(category) && this.Localisations[category].ContainsKey(key); }

        public void Add(string category) { if (!this.Has(category)) { this.Localisations.Add(category, new Dictionary<string, string>()); } }

        public void Add(string category, string key, string value)
        {
            if (!this.Has(category)) { this.Add(category); }
            this.Localisations[category][key] = value;
        }

        public void Substitute()
        {
            string[] cats = this.Localisations.Keys.ToArray();
            foreach (var cat in cats)
            {
                string[] keys = this.Localisations[cat].Keys.ToArray();
                foreach (var key in keys)
                {
                    string value = this.Localisations[cat][key];
                    foreach (var pair in this.Substitutions)
                    {
                        value = value.Replace(pair.Key, pair.Value);
                    }
                    this.Localisations[cat][key] = value;
                }
            }
        }
    }
}
