﻿#region File Header
// <copyright>
// Copyright (c) 2017 Jay Jeckel
//
// This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
// EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//
// <author>Jay Jeckel</author>
// <date>2017-04-26</date>
//
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace TmlGogPatcher.Utils
{
    /// <summary>
    /// Class that can parse an array of command line arguments and allow easy access to those arguments
    /// and their associated values.
    /// </summary>
    public class Arguments
    {
        /// <summary>
        /// Array containing the new line character used as a separator in internal processing.
        /// </summary>
        protected static readonly char[] NewLineSplitChar = new[] { '\n' };

        /// <summary>
        /// Array of characters that are used to separate arguments from values.
        /// </summary>
        protected static readonly char[] ArgValueSplitChars = new[] { '=', ':' };

        /// <summary>
        /// Array of characters that are used as argument prefixes.
        /// </summary>
        protected static readonly char[] ArgPrefixTrimChars = new[] { '-', '/' };

        /// <summary>
        /// Split the passed line of text on space characters that are not contained within a wrapping pair of quotes and
        /// return the modified text.
        /// </summary>
        /// <param name="line">Line of text to parse.</param>
        public static string[] SplitCommandLine(string line)
        {
            if (string.IsNullOrWhiteSpace(line)) { return new string[0]; }
            var builder = new StringBuilder(line);
            bool quoted = false;
            for (var index = 0; index < builder.Length; index++)
            {
                if (builder[index] == '"') { quoted = !quoted; }
                else if (builder[index] == ' ' && !quoted) { builder[index] = '\n'; }
            }

            string[] list = builder.ToString().Split(Arguments.NewLineSplitChar, StringSplitOptions.RemoveEmptyEntries);
            for (var index = 0; index < list.Length; index++) { list[index] = Arguments.RemoveOutterQuotePair(list[index]); }
            return list;
        }

        /// <summary>
        /// Remove the outter most pair of quote characters. Note that the quotes will only be removed if there are two
        /// or more quote characters and the last quote character is the final character of the string.
        /// </summary>
        /// <param name="text">Text from which to strip the quote pair.</param>
        public static string RemoveOutterQuotePair(string text)
        {
            if (string.IsNullOrWhiteSpace(text)) { return text; }
            int first = text.IndexOf('"');
            int last = text.LastIndexOf('"');
            if (first != -1 && first != last && last == text.Length - 1) { text = text.Remove(first, 1).Remove(last - 1, 1); }
            return text;
        }

        /// <summary>
        /// Create an instance of the Arguments class based on the passed arguments.
        /// </summary>
        /// <param name="arguments">Arguments to be parsed by the instance.</param>
        public Arguments(IEnumerable<string> arguments)
        {
            foreach (string argument in arguments)
            {
                string[] parts = argument.Split(Arguments.ArgValueSplitChars, 2);
                parts[0] = parts[0].TrimStart(Arguments.ArgPrefixTrimChars);

                if (parts.Length == 1) { this.AddFlag(parts[0]); }
                else { this.AddValues(parts[0], parts[1].Split(',')); }
            }
        }

        /// <summary>
        /// Dictionary which stores the arguments and their values.
        /// </summary>
        public Dictionary<string, Collection<string>> Args { get; } = new Dictionary<string, Collection<string>>();

        /// <summary>
        /// Count of arguments stored in the instance.
        /// </summary>
        public int Count { get { return this.Args.Count; } }
        
        /// <summary>
        /// Determine if the given argument is contained within the instance.
        /// </summary>
        /// <param name="argument">Argument to check for.</param>
        public bool Has(string argument) { return this.Args.ContainsKey(argument); }

        /// <summary>
        /// Remove the given argument if it is found within the instance.
        /// </summary>
        /// <param name="argument">Argument to remove.</param>
        public void Remove(string argument) { if (this.Has(argument)) { this.Args.Remove(argument); } }

        /// <summary>
        /// Add the value to the argument.
        /// </summary>
        /// <param name="argument">Argument to add the value under.</param>
        /// <param name="value">Value to add under the argument.</param>
        public void AddValue(string argument, string value)
        {
            if (!this.Has(argument)) { this.Args.Add(argument, new Collection<string>()); }
            this.Args[argument].Add(value);
        }

        /// <summary>
        /// Add multiple values to the argument.
        /// </summary>
        /// <param name="argument">Argument to add the values under.</param>
        /// <param name="values">Values to add under the argument.</param>
        public void AddValues(string argument, params string[] values)
        {
            if (!this.Has(argument)) { this.Args.Add(argument, new Collection<string>()); }
            foreach (string value in values) { this.Args[argument].Add(Arguments.RemoveOutterQuotePair(value.Trim(' '))); }
        }

        /// <summary>
        /// Add a flag argument.
        /// </summary>
        /// <param name="argument">Flag argument to add.</param>
        public void AddFlag(string argument) { if (!this.Has(argument)) { this.Args.Add(argument, null); } }

        /// <summary>
        /// Get the first value stored under the argument. If the argument does not exist, is a flag, or doesn't contain any values,
        /// then null will be returned.
        /// </summary>
        /// <param name="argument">Argument for which to get the first value.</param>
        public string GetValue(string argument) { return this.Has(argument) && this.Args[argument] != null && this.Args[argument].Count > 0 ? this.Args[argument][0] : null; }

        /// <summary>
        /// Get the value of the flag argument. If the argument does not exist or is not a flag, then false will be return,
        /// otherwise true will be returned.
        /// </summary>
        /// <param name="argument">Argument for which to get the flag value.</param>
        public bool GetFlag(string argument) { return this.Has(argument) && this.Args[argument] == null; }

        /// <summary>
        /// Get the values of the argument. If the argument does not exist or is a flag, then null will be returned.
        /// </summary>
        /// <param name="argument">Argument for which to get the values.</param>
        public string[] GetList(string argument) { return this.Has(argument) ? this.Args[argument].ToArray() : null; }

        /// <summary>
        /// Check if an argument is a flag value. If the argument was read in as a flag or doesn't exist,
        /// then true will be returned, otherwise false will be returned.
        /// </summary>
        /// <param name="argument">Argument to determine if it is a flag or not.</param>
        public bool IsFlag(string argument) { return !this.Has(argument) || this.Args[argument] == null; }

        /// <summary>
        /// Return a command line formatted string representation of the items contained in this object.
        /// The returned string will almost certainly not be the same as the original command line.
        /// Flags will be prefixed with a single dash (-) character, non-flag arguments will be prefixed
        /// with the slash (/) character, arguments will be separated from their values by the equal (=)
        /// character, and arguments with multiple values will have them joined by a comma.
        /// </summary>
        public override string ToString()
        {
            string output = string.Empty;
            foreach (string argument in this.Args.Keys) { if (this.IsFlag(argument)) { output += $"-{argument} "; } }
            foreach (string argument in this.Args.Keys) { if (!this.IsFlag(argument)) { output += $"/{argument}={string.Join(",", this.Args[argument])} "; } }
            return output.TrimEnd();
        }
    }
}
