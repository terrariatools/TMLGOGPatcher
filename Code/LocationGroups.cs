﻿using System.Collections.Generic;

namespace TmlGogPatcher
{
    public class LocationGroups
    {
        public Dictionary<string, LocationGroup> Items { get; set; } = new Dictionary<string, LocationGroup>();

        public string this[string category, string title] { get { return (!this.Has(category, title) ? null : this.Items[category][title]); } }

        public bool Has(string category) { return this.Items.ContainsKey(category); }

        public bool Has(string category, string title) { return this.Has(category) && this.Items[category].Has(title); }

        public void Add(string category, LocationGroup location) { this.Items[category] = location; }
    }
}
