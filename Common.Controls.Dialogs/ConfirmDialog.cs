﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Common.Controls.Dialogs
{
    public partial class ConfirmDialog : DialogForm
    {
        public ConfirmDialog()
        {
            this.InitializeComponent();
            this.ContentFont = this.Font;
        }

        public Font ContentFont { get; set; }

        public Color ContentBackColor { get { return this._panelContents.BackColor; } set { this._panelContents.BackColor = value; } }

        public void SetIcon(SystemIconEnum value)
        {
            Icon icon = null;
            switch (value)
            {
                case SystemIconEnum.Application: { icon = SystemIcons.Application; break; }
                case SystemIconEnum.Asterisk: { icon = SystemIcons.Asterisk; break; }
                case SystemIconEnum.Error: { icon = SystemIcons.Error; break; }
                case SystemIconEnum.Exclamation: { icon = SystemIcons.Exclamation; break; }
                case SystemIconEnum.Hand: { icon = SystemIcons.Hand; break; }
                case SystemIconEnum.Information: { icon = SystemIcons.Information; break; }
                case SystemIconEnum.Question: { icon = SystemIcons.Question; break; }
                case SystemIconEnum.Shield: { icon = SystemIcons.Shield; break; }
                case SystemIconEnum.Warning: { icon = SystemIcons.Warning; break; }
                case SystemIconEnum.WinLogo: { icon = SystemIcons.WinLogo; break; }
                default: { icon = null; break; }
            }
            this.Icon = icon;
        }

        public void AddButton(DialogResult value)
        {
            if (!Enum.IsDefined(typeof(DialogResult), value) || value == DialogResult.None) { throw new InvalidEnumArgumentException("value", (int)value, typeof(DialogResult)); }
            Button button = new Button();
            button.DialogResult = value;
            button.Font = this.Font;
            button.Margin = new Padding(2, 4, 2, 4);
            button.Size = new Size(75, 22);
            button.Text = Enum.GetName(typeof(DialogResult), value);
            int col = this._tableButtons.ColumnCount;
            int row = 0;
            this._tableButtons.ColumnCount += 1;
            this._tableButtons.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            this._tableButtons.Controls.Add(button, col, row);
        }

        public void AddButtons(params DialogResult[] values)
        {
            foreach (DialogResult value in values)
            {
                this.AddButton(value);
            }
        }

        public void AddContent(Font font, string text)
        {
            Label label = new Label();
            label.Font = font;
            label.Text = text;
            label.AutoSize = true;
            int col = 0;
            int row = this._tableContent.RowCount - 1;
            this._tableContent.RowCount += 1;
            this._tableContent.RowStyles.Insert(0, new RowStyle(SizeType.AutoSize));
            this._tableContent.Controls.Add(label, col, row);
        }

        public void AddContent(string text) { this.AddContent(this.ContentFont, text); }

        public void AddContent(Font font, string format, params object[] args) { this.AddContent(font, string.Format(format, args)); }

        public void AddContent(string format, params object[] args) { this.AddContent(this.ContentFont, string.Format(format, args)); }

        public void AddContent() { this.AddContent(string.Empty); }

        private void ConfirmDialog_Load(object sender, EventArgs e)
        {

        }
    }

    public enum SystemIconEnum : int
    {
        Application,
        Asterisk,
        Error,
        Exclamation,
        Hand,
        Information,
        Question,
        Shield,
        Warning,
        WinLogo
    }
}
