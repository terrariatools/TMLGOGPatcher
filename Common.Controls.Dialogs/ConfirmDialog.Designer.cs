﻿namespace Common.Controls.Dialogs
{
    partial class ConfirmDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._tableButtons = new System.Windows.Forms.TableLayoutPanel();
            this._tableContent = new System.Windows.Forms.TableLayoutPanel();
            this._panelContents = new System.Windows.Forms.Panel();
            this._tableLayout.SuspendLayout();
            this._panelContents.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayout
            // 
            this._tableLayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this._tableLayout.ColumnCount = 1;
            this._tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout.Controls.Add(this._tableButtons, 0, 1);
            this._tableLayout.Controls.Add(this._panelContents, 0, 0);
            this._tableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayout.Location = new System.Drawing.Point(0, 0);
            this._tableLayout.Margin = new System.Windows.Forms.Padding(0);
            this._tableLayout.Name = "_tableLayout";
            this._tableLayout.RowCount = 2;
            this._tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayout.Size = new System.Drawing.Size(284, 62);
            this._tableLayout.TabIndex = 0;
            // 
            // _tableButtons
            // 
            this._tableButtons.AutoSize = true;
            this._tableButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._tableButtons.ColumnCount = 1;
            this._tableButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableButtons.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this._tableButtons.Location = new System.Drawing.Point(1, 61);
            this._tableButtons.Margin = new System.Windows.Forms.Padding(0);
            this._tableButtons.Name = "_tableButtons";
            this._tableButtons.RowCount = 1;
            this._tableButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableButtons.Size = new System.Drawing.Size(282, 1);
            this._tableButtons.TabIndex = 0;
            // 
            // _tableContent
            // 
            this._tableContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tableContent.AutoSize = true;
            this._tableContent.ColumnCount = 1;
            this._tableContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableContent.Location = new System.Drawing.Point(0, 0);
            this._tableContent.Margin = new System.Windows.Forms.Padding(0);
            this._tableContent.MinimumSize = new System.Drawing.Size(20, 20);
            this._tableContent.Name = "_tableContent";
            this._tableContent.RowCount = 1;
            this._tableContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableContent.Size = new System.Drawing.Size(282, 20);
            this._tableContent.TabIndex = 1;
            // 
            // _panelContents
            // 
            this._panelContents.AutoScroll = true;
            this._panelContents.BackColor = System.Drawing.SystemColors.Window;
            this._panelContents.Controls.Add(this._tableContent);
            this._panelContents.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panelContents.Location = new System.Drawing.Point(1, 1);
            this._panelContents.Margin = new System.Windows.Forms.Padding(0);
            this._panelContents.Name = "_panelContents";
            this._panelContents.Size = new System.Drawing.Size(282, 59);
            this._panelContents.TabIndex = 1;
            // 
            // ConfirmDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 62);
            this.Controls.Add(this._tableLayout);
            this.MinimumSize = new System.Drawing.Size(300, 100);
            this.Name = "ConfirmDialog";
            this.Text = "ConfirmDialog";
            this.Load += new System.EventHandler(this.ConfirmDialog_Load);
            this._tableLayout.ResumeLayout(false);
            this._tableLayout.PerformLayout();
            this._panelContents.ResumeLayout(false);
            this._panelContents.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayout;
        private System.Windows.Forms.TableLayoutPanel _tableButtons;
        private System.Windows.Forms.TableLayoutPanel _tableContent;
        private System.Windows.Forms.Panel _panelContents;
    }
}