﻿using System.Drawing;
using System.Windows.Forms;

namespace Common.Controls.Dialogs
{
    public class DialogForm : Form
    {
        public DialogForm()
        {
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public void SetIcon(Bitmap image) { this.Icon = Icon.FromHandle(image.GetHicon()); }

        public void SetIcon(Icon icon) { this.Icon = icon; }
    }
}
