﻿using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
namespace Common.Controls.Dialogs
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private TableLayoutPanel _tableLayout;
        private PictureBox _imageLogo;
        private Label _labelProduct;
        private Label _labelVersion;
        private Label _labelCopyright;
        private Label _labelCompany;
        private TextBox _textDesc;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._tableDetails = new System.Windows.Forms.TableLayoutPanel();
            this._labelCompany = new System.Windows.Forms.Label();
            this._labelVersion = new System.Windows.Forms.Label();
            this._imageLogo = new System.Windows.Forms.PictureBox();
            this._linkEmail = new System.Windows.Forms.LinkLabel();
            this._labelCopyright = new System.Windows.Forms.Label();
            this._linkUrl = new System.Windows.Forms.LinkLabel();
            this._labelProduct = new System.Windows.Forms.Label();
            this._labelProductLabel = new System.Windows.Forms.Label();
            this._labelVersionLabel = new System.Windows.Forms.Label();
            this._labelCompanyLabel = new System.Windows.Forms.Label();
            this._labelEmailLabel = new System.Windows.Forms.Label();
            this._labelUrlLabel = new System.Windows.Forms.Label();
            this._buttonClose = new System.Windows.Forms.Button();
            this._textDesc = new System.Windows.Forms.TextBox();
            this._tabs = new System.Windows.Forms.TabControl();
            this._tabGeneral = new System.Windows.Forms.TabPage();
            this._imageList = new System.Windows.Forms.ImageList(this.components);
            this._tableLayout.SuspendLayout();
            this._tableDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._imageLogo)).BeginInit();
            this._tabs.SuspendLayout();
            this._tabGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayout
            // 
            this._tableLayout.ColumnCount = 1;
            this._tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout.Controls.Add(this._tableDetails, 0, 0);
            this._tableLayout.Controls.Add(this._buttonClose, 0, 2);
            this._tableLayout.Controls.Add(this._textDesc, 0, 1);
            this._tableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayout.Location = new System.Drawing.Point(0, 0);
            this._tableLayout.Margin = new System.Windows.Forms.Padding(0);
            this._tableLayout.Name = "_tableLayout";
            this._tableLayout.RowCount = 3;
            this._tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableLayout.Size = new System.Drawing.Size(376, 176);
            this._tableLayout.TabIndex = 0;
            // 
            // _tableDetails
            // 
            this._tableDetails.AutoSize = true;
            this._tableDetails.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._tableDetails.ColumnCount = 3;
            this._tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableDetails.Controls.Add(this._labelCompany, 2, 2);
            this._tableDetails.Controls.Add(this._labelVersion, 2, 1);
            this._tableDetails.Controls.Add(this._imageLogo, 0, 0);
            this._tableDetails.Controls.Add(this._linkEmail, 2, 4);
            this._tableDetails.Controls.Add(this._labelCopyright, 0, 5);
            this._tableDetails.Controls.Add(this._linkUrl, 2, 3);
            this._tableDetails.Controls.Add(this._labelProduct, 2, 0);
            this._tableDetails.Controls.Add(this._labelProductLabel, 1, 0);
            this._tableDetails.Controls.Add(this._labelVersionLabel, 1, 1);
            this._tableDetails.Controls.Add(this._labelCompanyLabel, 1, 2);
            this._tableDetails.Controls.Add(this._labelEmailLabel, 1, 4);
            this._tableDetails.Controls.Add(this._labelUrlLabel, 1, 3);
            this._tableDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableDetails.Location = new System.Drawing.Point(0, 0);
            this._tableDetails.Margin = new System.Windows.Forms.Padding(0);
            this._tableDetails.Name = "_tableDetails";
            this._tableDetails.RowCount = 6;
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._tableDetails.Size = new System.Drawing.Size(376, 102);
            this._tableDetails.TabIndex = 1;
            // 
            // _labelCompany
            // 
            this._labelCompany.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelCompany.AutoSize = true;
            this._labelCompany.Location = new System.Drawing.Point(140, 36);
            this._labelCompany.Margin = new System.Windows.Forms.Padding(2);
            this._labelCompany.MaximumSize = new System.Drawing.Size(0, 17);
            this._labelCompany.Name = "_labelCompany";
            this._labelCompany.Size = new System.Drawing.Size(82, 13);
            this._labelCompany.TabIndex = 5;
            this._labelCompany.Text = "Company Name";
            this._labelCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _labelVersion
            // 
            this._labelVersion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelVersion.AutoSize = true;
            this._labelVersion.Location = new System.Drawing.Point(140, 19);
            this._labelVersion.Margin = new System.Windows.Forms.Padding(2);
            this._labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this._labelVersion.Name = "_labelVersion";
            this._labelVersion.Size = new System.Drawing.Size(42, 13);
            this._labelVersion.TabIndex = 3;
            this._labelVersion.Text = "Version";
            this._labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _imageLogo
            // 
            this._imageLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this._imageLogo.Location = new System.Drawing.Point(7, 6);
            this._imageLogo.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this._imageLogo.MaximumSize = new System.Drawing.Size(64, 64);
            this._imageLogo.MinimumSize = new System.Drawing.Size(16, 16);
            this._imageLogo.Name = "_imageLogo";
            this._tableDetails.SetRowSpan(this._imageLogo, 5);
            this._imageLogo.Size = new System.Drawing.Size(64, 64);
            this._imageLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this._imageLogo.TabIndex = 12;
            this._imageLogo.TabStop = false;
            // 
            // _linkEmail
            // 
            this._linkEmail.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._linkEmail.AutoSize = true;
            this._linkEmail.Location = new System.Drawing.Point(140, 70);
            this._linkEmail.Margin = new System.Windows.Forms.Padding(2);
            this._linkEmail.Name = "_linkEmail";
            this._linkEmail.Size = new System.Drawing.Size(32, 13);
            this._linkEmail.TabIndex = 9;
            this._linkEmail.TabStop = true;
            this._linkEmail.Text = "Email";
            this._linkEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _labelCopyright
            // 
            this._labelCopyright.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelCopyright.AutoSize = true;
            this._tableDetails.SetColumnSpan(this._labelCopyright, 3);
            this._labelCopyright.Location = new System.Drawing.Point(2, 87);
            this._labelCopyright.Margin = new System.Windows.Forms.Padding(2);
            this._labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
            this._labelCopyright.Name = "_labelCopyright";
            this._labelCopyright.Size = new System.Drawing.Size(51, 13);
            this._labelCopyright.TabIndex = 10;
            this._labelCopyright.Text = "Copyright";
            this._labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _linkUrl
            // 
            this._linkUrl.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._linkUrl.AutoSize = true;
            this._linkUrl.Location = new System.Drawing.Point(140, 53);
            this._linkUrl.Margin = new System.Windows.Forms.Padding(2);
            this._linkUrl.Name = "_linkUrl";
            this._linkUrl.Size = new System.Drawing.Size(20, 13);
            this._linkUrl.TabIndex = 7;
            this._linkUrl.TabStop = true;
            this._linkUrl.Text = "Url";
            this._linkUrl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._linkUrl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Link_LinkClicked);
            // 
            // _labelProduct
            // 
            this._labelProduct.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._labelProduct.AutoSize = true;
            this._labelProduct.Location = new System.Drawing.Point(140, 2);
            this._labelProduct.Margin = new System.Windows.Forms.Padding(2);
            this._labelProduct.MaximumSize = new System.Drawing.Size(0, 17);
            this._labelProduct.Name = "_labelProduct";
            this._labelProduct.Size = new System.Drawing.Size(75, 13);
            this._labelProduct.TabIndex = 1;
            this._labelProduct.Text = "Product Name";
            this._labelProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _labelProductLabel
            // 
            this._labelProductLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._labelProductLabel.AutoSize = true;
            this._labelProductLabel.Location = new System.Drawing.Point(88, 2);
            this._labelProductLabel.Name = "_labelProductLabel";
            this._labelProductLabel.Size = new System.Drawing.Size(47, 13);
            this._labelProductLabel.TabIndex = 0;
            this._labelProductLabel.Text = "Product:";
            // 
            // _labelVersionLabel
            // 
            this._labelVersionLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._labelVersionLabel.AutoSize = true;
            this._labelVersionLabel.Location = new System.Drawing.Point(90, 19);
            this._labelVersionLabel.Name = "_labelVersionLabel";
            this._labelVersionLabel.Size = new System.Drawing.Size(45, 13);
            this._labelVersionLabel.TabIndex = 2;
            this._labelVersionLabel.Text = "Version:";
            // 
            // _labelCompanyLabel
            // 
            this._labelCompanyLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._labelCompanyLabel.AutoSize = true;
            this._labelCompanyLabel.Location = new System.Drawing.Point(81, 36);
            this._labelCompanyLabel.Name = "_labelCompanyLabel";
            this._labelCompanyLabel.Size = new System.Drawing.Size(54, 13);
            this._labelCompanyLabel.TabIndex = 4;
            this._labelCompanyLabel.Text = "Company:";
            // 
            // _labelEmailLabel
            // 
            this._labelEmailLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._labelEmailLabel.AutoSize = true;
            this._labelEmailLabel.Location = new System.Drawing.Point(100, 70);
            this._labelEmailLabel.Name = "_labelEmailLabel";
            this._labelEmailLabel.Size = new System.Drawing.Size(35, 13);
            this._labelEmailLabel.TabIndex = 8;
            this._labelEmailLabel.Text = "Email:";
            // 
            // _labelUrlLabel
            // 
            this._labelUrlLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._labelUrlLabel.AutoSize = true;
            this._labelUrlLabel.Location = new System.Drawing.Point(86, 53);
            this._labelUrlLabel.Name = "_labelUrlLabel";
            this._labelUrlLabel.Size = new System.Drawing.Size(49, 13);
            this._labelUrlLabel.TabIndex = 6;
            this._labelUrlLabel.Text = "Website:";
            // 
            // _buttonClose
            // 
            this._buttonClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._buttonClose.Location = new System.Drawing.Point(150, 152);
            this._buttonClose.Margin = new System.Windows.Forms.Padding(2);
            this._buttonClose.Name = "_buttonClose";
            this._buttonClose.Size = new System.Drawing.Size(75, 22);
            this._buttonClose.TabIndex = 0;
            this._buttonClose.Text = "&Close";
            this._buttonClose.Visible = false;
            // 
            // _textDesc
            // 
            this._textDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._textDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this._textDesc.Location = new System.Drawing.Point(3, 105);
            this._textDesc.Multiline = true;
            this._textDesc.Name = "_textDesc";
            this._textDesc.ReadOnly = true;
            this._textDesc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._textDesc.Size = new System.Drawing.Size(370, 42);
            this._textDesc.TabIndex = 2;
            this._textDesc.Text = "Description";
            // 
            // _tabs
            // 
            this._tabs.Controls.Add(this._tabGeneral);
            this._tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabs.ImageList = this._imageList;
            this._tabs.Location = new System.Drawing.Point(0, 0);
            this._tabs.Margin = new System.Windows.Forms.Padding(0);
            this._tabs.Name = "_tabs";
            this._tabs.SelectedIndex = 0;
            this._tabs.Size = new System.Drawing.Size(384, 202);
            this._tabs.TabIndex = 1;
            // 
            // _tabGeneral
            // 
            this._tabGeneral.BackColor = System.Drawing.SystemColors.Control;
            this._tabGeneral.Controls.Add(this._tableLayout);
            this._tabGeneral.Location = new System.Drawing.Point(4, 22);
            this._tabGeneral.Margin = new System.Windows.Forms.Padding(0);
            this._tabGeneral.Name = "_tabGeneral";
            this._tabGeneral.Size = new System.Drawing.Size(376, 176);
            this._tabGeneral.TabIndex = 0;
            this._tabGeneral.Text = "General";
            // 
            // _imageList
            // 
            this._imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._imageList.ImageSize = new System.Drawing.Size(16, 16);
            this._imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._buttonClose;
            this.ClientSize = new System.Drawing.Size(384, 202);
            this.Controls.Add(this._tabs);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "AboutDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About Application";
            this.Load += new System.EventHandler(this.AboutDialog_Load);
            this._tableLayout.ResumeLayout(false);
            this._tableLayout.PerformLayout();
            this._tableDetails.ResumeLayout(false);
            this._tableDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._imageLogo)).EndInit();
            this._tabs.ResumeLayout(false);
            this._tabGeneral.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private LinkLabel _linkUrl;
        private LinkLabel _linkEmail;
        private TableLayoutPanel _tableDetails;
        private Label _labelProductLabel;
        private Label _labelVersionLabel;
        private Label _labelCompanyLabel;
        private Label _labelUrlLabel;
        private Label _labelEmailLabel;
        private Button _buttonClose;
        private TabControl _tabs;
        private TabPage _tabGeneral;
        private ImageList _imageList;
    }
}