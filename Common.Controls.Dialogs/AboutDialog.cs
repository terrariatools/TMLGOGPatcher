﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Common.Reflection;

namespace Common.Controls.Dialogs
{
    public partial class AboutDialog : DialogForm
    {
        public AboutDialog()
        {
            this.InitializeComponent();
            this.UrlControl.Links.Clear();
            this.EmailControl.Links.Clear();
        }

        public AboutDialog(Assembly target)
            : this()
        {
            this.Target = target;
        }

        public Assembly Target
        {
            get { return this._target; }
            set
            {
                this._target = value;

                this.UpdateProduct();
                this.UpdateVersion();
                this.UpdateCompany();
                this.UpdateCopyright();
                this.UpdateDescription();
            }
        }
        private Assembly _target = null;

#region "Controls"

        public Label ProductControl { get { return this._labelProduct; } }

        public Label VersionControl { get { return this._labelVersion; } }

        public Label CompanyControl { get { return this._labelCompany; } }

        public Label CopyrightControl { get { return this._labelCopyright; } }

        public TextBox DescriptionControl { get { return this._textDesc; } }

        public PictureBox LogoControl { get { return this._imageLogo; } }

        public LinkLabel UrlControl { get { return this._linkUrl; } }

        public LinkLabel EmailControl { get { return this._linkEmail; } }

#endregion

#region "Values"

        public string TitleValue { get { return (this.Target == null ? "?" : this.Target.GetTitle()); } }

        public string ProductValue { get { return (this.Target == null ? "?" : this.Target.GetProduct()); } }

        public string VersionValue { get { return (this.Target == null ? "0.0.0.0" : this.Target.GetVersion()); } }

        public string CompanyValue { get { return (this.Target == null ? "?" : this.Target.GetCompany()); } }

        public string CopyrightValue { get { return (this.Target == null ? "?" : this.Target.GetCopyright()); } }

        public string DescriptionValue { get { return (this.Target == null ? "?" : this.Target.GetDescription()); } }

#endregion

#region "Visibles"

        public bool ProductVisible
        {
            get { return this._productVisible; }
            set
            {
                this._productVisible = value;
                this.UpdateProduct();
            }
        }
        protected bool _productVisible = true;

        public bool VersionVisible
        {
            get { return this._versionVisible; }
            set
            {
                this._versionVisible = value;
                this.UpdateVersion();
            }
        }
        protected bool _versionVisible = true;

        public bool CompanyVisible
        {
            get { return this._companyVisible; }
            set
            {
                this._companyVisible = value;
                this.UpdateCompany();
            }
        }
        protected bool _companyVisible = true;

        public bool CopyrightVisible
        {
            get { return this._copyrightVisible; }
            set
            {
                this._copyrightVisible = value;
                this.UpdateCopyright();
            }
        }
        protected bool _copyrightVisible = true;

        public bool DescriptionVisible
        {
            get { return this._descriptionVisible; }
            set
            {
                this._descriptionVisible = value;
                this.UpdateDescription();
            }
        }
        protected bool _descriptionVisible = true;

        public bool LogoVisible
        {
            get { return this._logoVisible; }
            set
            {
                this._logoVisible = value;
                this.UpdateLogo();
            }
        }
        protected bool _logoVisible = true;

        public bool UrlVisible
        {
            get { return this._urlVisible; }
            set
            {
                this._urlVisible = value;
                this.UpdateUrl();
            }
        }
        protected bool _urlVisible = true;

        public bool EmailVisible
        {
            get { return this._emailVisible; }
            set
            {
                this._emailVisible = value;
                this.UpdateEmail();
            }
        }
        protected bool _emailVisible = true;

#endregion

#region "Formats"

        public string ProductFormat
        {
            get { return this._productFormat; }
            set
            {
                this._productFormat = value;
                this.UpdateProduct();
            }
        }
        protected string _productFormat = "{0}";

        public string VersionFormat
        {
            get { return this._versionFormat; }
            set
            {
                this._versionFormat = value;
                this.UpdateVersion();
            }
        }
        protected string _versionFormat = "{0}";

        public string CompanyFormat
        {
            get { return this._companyFormat; }
            set
            {
                this._companyFormat = value;
                this.UpdateCompany();
            }
        }
        protected string _companyFormat = "{0}";

        public string CopyrightFormat
        {
            get { return this._copyrightFormat; }
            set
            {
                this._copyrightFormat = value;
                this.UpdateCopyright();
            }
        }
        protected string _copyrightFormat = "{0}";

        public string DescriptionFormat
        {
            get { return this._descriptionFormat; }
            set
            {
                this._descriptionFormat = value;
                this.UpdateDescription();
            }
        }
        protected string _descriptionFormat = "{0}";

#endregion

#region "Texts"

        public string ProductText { get { return string.Format(this.ProductFormat, this.ProductValue); } }

        public string VersionText { get { return string.Format(this.VersionFormat, this.VersionValue); } }

        public string CompanyText { get { return string.Format(this.CompanyFormat, this.CompanyValue); } }

        public string CopyrightText { get { return string.Format(this.CopyrightFormat, this.CopyrightValue); } }

        public string DescriptionText { get { return string.Format(this.DescriptionFormat, this.DescriptionValue); } }

#endregion

#region "Shown"

        public bool ProductShown { get { return this.ProductVisible && !string.IsNullOrWhiteSpace(this.ProductText); } }

        public bool VersionShown { get { return this.VersionVisible && !string.IsNullOrWhiteSpace(this.VersionText); } }

        public bool CompanyShown { get { return this.CompanyVisible && !string.IsNullOrWhiteSpace(this.CompanyText); } }

        public bool CopyrightShown { get { return this.CopyrightVisible && !string.IsNullOrWhiteSpace(this.CopyrightText); } }

        public bool DescriptionShown { get { return this.DescriptionVisible && !string.IsNullOrWhiteSpace(this.DescriptionText); } }

        public bool LogoShown { get { return this.LogoVisible && this.Logo != null; } }

        public bool UrlShown { get { return this.UrlVisible && this.UrlControl.Links.Count > 0; } }

        public bool EmailShown { get { return this.EmailVisible && this.EmailControl.Links.Count > 0; } }

#endregion

#region "Updates"

        public void UpdateProduct()
        {
            bool visible = this.ProductShown;
            this._labelProductLabel.Visible = visible;
            this.ProductControl.Visible = visible;
            this.ProductControl.Text = this.ProductText;
        }

        public void UpdateVersion()
        {
            bool visible = this.VersionShown;
            this._labelVersionLabel.Visible = visible;
            this.VersionControl.Visible = visible;
            this.VersionControl.Text = this.VersionText;
        }

        public void UpdateCompany()
        {
            bool visible = this.CompanyShown;
            this._labelCompanyLabel.Visible = visible;
            this.CompanyControl.Visible = visible;
            this.CompanyControl.Text = this.CompanyText;
        }

        public void UpdateCopyright()
        {
            bool visible = this.CopyrightShown;
            this.CopyrightControl.Visible = visible;
            this.CopyrightControl.Text = this.CopyrightText;
        }

        public void UpdateDescription()
        {
            bool visible = this.DescriptionShown;
            this.DescriptionControl.Visible = visible;
            this.DescriptionControl.Text = this.DescriptionText;
            this.DescriptionControl.Focus();
            this.DescriptionControl.Select(0, 0);
        }

        public void UpdateLogo()
        {
            bool visible = this.LogoShown;
            this.LogoControl.Visible = visible;
        }

        public void UpdateUrl()
        {
            bool visible = this.UrlShown;
            this._labelUrlLabel.Visible = visible;
            this.UrlControl.Visible = visible;
        }

        public void UpdateEmail()
        {
            bool visible = this.EmailShown;
            this._labelEmailLabel.Visible = visible;
            this.EmailControl.Visible = visible;
        }

#endregion

        public Image Logo
        {
            get { return this._imageLogo.Image; }
            set
            {
                this._imageLogo.Image = value;
                this.UpdateLogo();
            }
        }

        public void SetUrl(string url, string text = null)
        {
            LinkLabel.Link link = new LinkLabel.Link();
            link.LinkData = url;
            this.UrlControl.Links.Clear();
            this.UrlControl.Links.Add(link);
            this.UrlControl.Text = (string.IsNullOrWhiteSpace(text) ? url : text);
            this.UpdateUrl();
        }

        public void SetEmail(string url, string text = null)
        {
            LinkLabel.Link link = new LinkLabel.Link();
            link.LinkData = "mailto:" + url;
            this.EmailControl.Links.Clear();
            this.EmailControl.Links.Add(link);
            this.EmailControl.Text = (string.IsNullOrWhiteSpace(text) ? url : text);
            this.UpdateEmail();
        }

        public void AddPage(TabPage page)
        {
            page.Margin = Padding.Empty;
            page.Padding = Padding.Empty;
            this._tabs.TabPages.Add(page);
        }

        public void AddControlPage(string title, Control control, Image image = null, DockStyle dock = DockStyle.Fill)
        {
            this._tabs.ImageList.Images.Add(title, new Bitmap(image));
            TabPage page = new TabPage(title);
            page.Controls.Add(control);
            this.AddPage(page);
            page.ImageKey = title;

            control.Dock = DockStyle.Fill;
        }

        public void AddFileTextPage(string title, string path, Image image = null)
        {
            this.AddTextPage(title, File.ReadAllText(path), image);
        }

        public void AddTextPage(string title, string text, Image image = null)
        {
            TextBox box = new TextBox();
            this.AddControlPage(title, box, image);

            box.BorderStyle = BorderStyle.None;
            box.Multiline = true;
            box.ReadOnly = true;
            box.WordWrap = true;
            box.ScrollBars = ScrollBars.Both;
            box.Text = text;
            box.Select(0, 0);
        }

        public void AddFileHtmlPage(string title, Image image, string url)
        {
            WebBrowser box = new WebBrowser();
            this.AddControlPage(title, box, image);

            box.Navigate(url);
            box.Navigating += new WebBrowserNavigatingEventHandler(this.WebBrowserNavigating);
        }

        private void WebBrowserNavigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            e.Cancel = true;
            System.Diagnostics.Process.Start(e.Url.AbsoluteUri);
        }

        private void AboutDialog_Load(object sender, EventArgs e)
        {
            if (this.Target == null) { throw new InvalidOperationException("The Target property must be set before the form is loaded."); }

            this.Text = string.Format("About {0} v{1}", this.TitleValue, this.VersionValue);

            this._tabs.ImageList.Images.Add("General", this.Logo);
            this._tabGeneral.ImageKey = "General";

            this.UpdateProduct();
            this.UpdateVersion();
            this.UpdateCompany();
            this.UpdateCopyright();
            this.UpdateDescription();
            this.UpdateLogo();
            this.UpdateUrl();
            this.UpdateEmail();
        }

        private void Link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }
    }
}
