The TML GOG Patcher provides a simple method for applying [tModLoader (TML)](https://forums.terraria.org/index.php?threads/1-3-tmodloader-a-modding-api.23726/) patches to the [Good Old Games (GOG) versions of Terraria](https://www.gog.com/game/terraria).

This application provides an easy way to install the client and server patches, as well as required dependency files, to allow those who own the GOG version of Terraria to create and utilize tModLoader mods. The process is straight forward: enter or browse for the directory containing the Terraria.exe and TerrariaServer.exe files, select the desired options, click the Install button, and wait for the installation process to complete. The actions taken during installation are displayed in the text box at the bottom of the application as well as written to a log file that can be accessed via the View Log button at the top left.

Additionally, several buttons are provided at the top right which allow quick access to various TMLGOGPatcher, Terraria, and tModLoader folders (such as Patches, Mods, Players, and Worlds) and web pages, as well as the application About dialog which contains information about the application and licenses associated with it and the libraries and assets it makes use of. Additionally, next to the View Log button, there is a Patch Tools dropdown menu that allows the user to download a TML GOG Patch files. Lastly, on Windows, there are two buttons next to the Patch Tools menu that will allow the user to start Terraria (either from the unpatched client file or the backup client file) and tModLoader (either from the patched client file or the generated TML client file).

--------------------------------------------------------------------------------

## Known Bugs

* On Linux, the logging text box does not always update properly when resizing or scrolling, resulting in black lines being drawn across the box at weird positions and obscuring the text. A solution is being looked into, but for now use the View Log button and read the log file to determine success or failure of installation.
* On Linux, occasionally the application will lock up when patch files are added or removed from the patches folder. There is currently no solution to this issue, so for now if it happens, then restart the application.
* On Mac, the Patch Tools Download dialog does not work. A solution has not been found yet, so for now please manually download patch files from the TML GOG Patcher Website or Wiki, which can be found through the Visit Webpages menu, with your browser and manually place them in the patches folder, which can be found through the Open Folders menu.
* The Linux and Mac interfaces don't look great. This will be solved at some point in the future, but for now they are acceptable.

--------------------------------------------------------------------------------

## Application Features

* Control which aspects of TML is installed: client, server, and/or dependency files.
* Control if the Terraria client and/or server is overwritten by TML or if new exe files are created.
* Keeps a log of installation attempts to help with trouble shooting should problems occur.
* Provides easy access to TML GOG Patcher, Terraria, and tModLoader folders and websites.
* Most important of all, the Terraria files are verified before and after installation to ensure that the proper patch is applied and that it is applied successfully.

--------------------------------------------------------------------------------

## Installation Options

* Terraria and TML version selection dropdowns allow the user to select which version of Terraria is to be patched and which version of tModLoader it is to be patched with. These options will be automatically updated when patches are added or removed from the patches directory and will be set to the best matching patched if one is found. The ability to set non-matching values is allowed in case the version number of the Terraria files is not updated appropriately, which has happened occasionally with past updates to the game.
* Overwrite Client and Overwrite Server check boxes control whether the Terraria.exe and TerrariaServer.exe files are patched directly or if tModLoader.exe and tModLoaderServer.exe files are generated instead. The default on Windows is to overwrite Terraria.exe and to generate tModLoaderServer.exe. On Linux and Mac, the default is to overwrite both the client and server and it is recommended to leave both selected as there is currently no mechanism to launch TML if the original files are not overwritten.
* Install Client, Install Server, and Install Dependencies check boxes allow the user to install all or specific pieces of TML. This is useful if the user needs to reinstall lost dependency files, is just patching a server, or in other similar situations, though generally one will want all three boxes checked.

--------------------------------------------------------------------------------

## Usage Guide

* The first step is to download the TML GOG Patcher application for your operating system from the download links below: [Downloads](http://forums.terraria.org/index.php?threads/x.50413/#downloads)
* After downloading the application, you will need to download the patch for the version of Terraria that you are currently using from the links below: [Patch Files](http://forums.terraria.org/index.php?threads/x.50413/#patches)
If you don't know what version you are using, then you can start Terraria and find the version in the bottom left corner of the main menu screen or you can start the TML GOG Patcher, browse to the location of the Terraria.exe file, and you will see the version number in the status bar at the bottom of the application. **NOTE**: _Do NOT unzip the downloaded zip file, simply place the zip file itself in the patches direction._
* Once the patch file is downloaded, it needs to be placed in the TML GOG Patcher's patches folder. The patches folder is located in the TMLGOGPatcher folder which is located along side Terraria's players and worlds folders. The patches folder can also be opened from the Open Folders menu in the TML GOG Patcher's toolbar.
* Once the patch file is in the patches folder, TML GOG Patcher will see it if the application is already running, otherwise start the application.
* Lastly, select the Installation Options and click the Install button and the application will patch up Terraria with TML and then you should be ready to enjoy Terraria with mods.

--------------------------------------------------------------------------------

## Windows Installation Instructions

1. Download and unzip Windows TMLGOGPatcher_WindowsRelease.zip from below.
1. The TMLGOGPatcher.exe should now be ready to run by double-clicking.

--------------------------------------------------------------------------------

## Mac Installation Instructions

1. Download and unzip Mac TMLGOGPatcher_MacRelease.zip from below.
1. Download and install the mono package ([Instructions](http://www.mono-project.com/docs/getting-started/install/mac/)): [Download](http://www.mono-project.com/download/#download-mac)
1. TMLGOGPatcher.exe should now be ready to run through the terminal:
    ```
    mono <location>/TMLGOGPatcher.exe
    ```

--------------------------------------------------------------------------------

## Linux (Ubuntu) Installation Instructions

1. Download and unzip Linux TMLGOGPatcher_LinuxRelease.zip from below.
1. Install mono package through the terminal ([Instructions](http://www.mono-project.com/docs/getting-started/install/linux/)):
    ```
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
    echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
    sudo apt-get update
    echo "deb http://download.mono-project.com/repo/debian wheezy-apache24-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list
    ```
1. TMLGOGPatcher.exe should now be ready to run through the terminal:
    ```
	mono <location>/TMLGOGPatcher.exe
    ```

--------------------------------------------------------------------------------

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png) This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/). Utilised assets are released under various licenses, see the included Lincenses.txt file for details.
