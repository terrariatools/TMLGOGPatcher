﻿using System;
using System.Windows.Forms;
using TmlGogPatcher.Utils;

namespace TmlGogPatcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Arguments arguments = (args.Length > 0 ? new Arguments(args) : null);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(arguments == null ? new MainForm() : new MainForm(arguments));
        }
    }
}
