TMLGOGPatcher (https://gitlab.com/terrariatools/TMLGOGPatcher)

Copyright � 2016 Jay Jeckel

Creative Commons Attribution-Noncommercial-Share Alike 4.0 International (https://creativecommons.org/licenses/by-nc-sa/4.0/)

You are free to:

    Share � copy and redistribute the material in any medium or format
    Adapt � remix, transform, and build upon the material

    The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

    Attribution � You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

    NonCommercial � You may not use the material for commercial purposes.

    ShareAlike � If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

    No additional restrictions � You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Notices:

    You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
    No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

--------------------------------------------------------------------------------

ICSharpCode.SharpZipLib.dll (https://github.com/icsharpcode/SharpZipLib)

The MIT License (MIT) (https://opensource.org/licenses/MIT)

Copyright 2001-2010 Mike Krueger, John Reilly

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--------------------------------------------------------------------------------

Ookii.Dialogs.dll (http://www.ookii.org/software/dialogs/)

License agreement for Ookii.Dialogs. (https://github.com/marklagendijk/WinLess/blob/master/packages/Ookii.Dialogs.1.0/license.txt)

Copyright � Sven Groot (Ookii.org) 2009
All rights reserved.

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

1) Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
2) Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
3) Neither the name of the ORGANIZATION nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--------------------------------------------------------------------------------

About Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-help-about-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

Licenses Icon (http://www.iconarchive.com/show/torrent-icons-by-aha-soft/copyright-icon.html)

Creative Commons Attribution 4.0 International (https://creativecommons.org/licenses/by/4.0/)

--------------------------------------------------------------------------------

Open Folder Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-document-open-folder-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

Browse Folders Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-document-open-folder-icon.html and http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-page-zoom-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

View Log Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Mimetypes-text-x-log-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

Internet Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Categories-applications-internet-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

Valid Folder Icon (http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-dialog-ok-apply-icon.html)

GNU Lesser General Public License (https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)

--------------------------------------------------------------------------------

Invalid Folder Icon (http://www.iconarchive.com/show/basic-icons-by-pixelmixer/warning-icon.html)

Freeware

--------------------------------------------------------------------------------

Non-existent Folder Icon (http://www.iconarchive.com/show/multipurpose-alphabet-icons-by-hydrattz/Letter-X-red-icon.html)

Free for non-commercial use.

--------------------------------------------------------------------------------

tModLoader (https://github.com/bluemagic123/tModLoader/blob/master/LICENSE)

Copyright � 2015 Nathaniel Minsung Kim

--------------------------------------------------------------------------------

Terraria (http://terraria.org)

Copyright � 2016 Re-Logic

--------------------------------------------------------------------------------
