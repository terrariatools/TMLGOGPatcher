﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TMLGOGPatcher")]
[assembly: AssemblyDescription("The TML GOG Patcher provides a simple method for applying tModLoader (TML) patches to the Good Old Games (GOG) versions of Terraria.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vinland Solutions")]
[assembly: AssemblyProduct("TMLGOGPatcher")]
[assembly: AssemblyCopyright("Copyright © 2016 Jay Jeckel")]
[assembly: AssemblyTrademark("Copyright © 2016 Jay Jeckel")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("82675fa7-2152-4b5f-8040-b9c530bf49ac")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.2.0")]
[assembly: AssemblyFileVersion("1.1.2.0")]
